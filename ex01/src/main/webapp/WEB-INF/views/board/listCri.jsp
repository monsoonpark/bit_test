<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page session="false" %>
<%@ include file="../include/header.jsp" %>
<script>
	var result="${msg}";
	if(result=='success'){
		alert("처리 완료");
	}
</script>
<table class="table table-bordered">
	<tr>
		<th style="width:10px ">BNO</th>
		<th>TITLE</th>
		<th>WRITER</th>
		<th>REGDATE</th>
		<th style="width: 40px">VIEWCNT</th>
	</tr>
	<c:forEach items="${list }" var="vo">
		<tr>
			<td>${vo.bno }</td>
			<td><a href='/board/read?bno=${vo.bno}'>${vo.title }</a></td>
			<td>${vo.writer }</td>
			<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${vo.regdate }"/></td>
			<td><span class="badge bg-red">${vo.viewcnt }</span></td>
		</tr>
	</c:forEach>
	
</table>
	
<%@ include file="../include/footer.jsp" %>