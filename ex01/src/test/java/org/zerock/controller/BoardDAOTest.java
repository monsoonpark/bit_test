package org.zerock.controller;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.queryParam;

import java.util.List;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import org.zerock.domain.BoardVO;
import org.zerock.domain.Criteria;
import org.zerock.domain.SearchCriteria;
import org.zerock.persistence.BoardDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations= {"file:src/main/webapp/WEB-INF/spring/**/root-context.xml"})
public class BoardDAOTest {

	@Inject
	private BoardDAO dao;
	
	private static Logger logger=LoggerFactory.getLogger(BoardDAOTest.class);
	
	@Test
	public void testDynamic1()throws Exception{
		SearchCriteria cri=new SearchCriteria();
		cri.setPage(1);
		cri.setKeyword("테스트");
		cri.setSearchType("t");
		
		logger.info("---------------------------------");
		List<BoardVO> list=dao.listSerach(cri);
		for(BoardVO boardVO: list) {
			logger.info(boardVO.getBno()+": "+boardVO.getTitle());
		}
		logger.info("=================================");
		logger.info("COUNT: "+dao.listSearchCount(cri));
		
	}
	
	public void testURI2()throws Exception {
		UriComponents uriComponents=UriComponentsBuilder.newInstance()
				.path("/{board}/{page}")
				.queryParam("bno", 12)
				.queryParam("perPageNum", 20)
				.build()
				.expand("board","read")
				.encode();
					
		logger.info("/board/read?bno=12&perPageNum=20");
		logger.info(uriComponents.toString());
	}
	public void testURI()throws Exception {
		UriComponents uriComponents=UriComponentsBuilder.newInstance()
				.path("/board/read")
				.queryParam("bno", 12)
				.queryParam("perPageNum", 20)
				.build();
		
		logger.info("/board/read?bno=12&perPageNum=20");
		logger.info(uriComponents.toString());
	}
	
	public void testListCriteria() throws Exception{
		Criteria cri=new Criteria();
		cri.setPage(2);
		cri.setPerPageNum(20);
		List<BoardVO> list=dao.listCriteria(cri);
		for(BoardVO boardVO: list){
			logger.info(boardVO.getBno()+" : "+boardVO.getTitle());
		}
	}
	
	public void testListPage() throws Exception{
		int page=3;
		List<BoardVO> list=dao.listPage(page);
		for(BoardVO boardVO: list){
			logger.info(boardVO.getBno()+" : "+boardVO.getTitle());
		}
	}
	
	public void testCreate()throws Exception {
		BoardVO board=new BoardVO();
		board.setTitle("새로운 글");
		board.setContent("새로운 내용");
		board.setWriter("user00");
		dao.create(board);
		//fail("Not yet implemented");
	}
	
	
	public void testRead()throws Exception{
		logger.info(dao.read(5).toString());
	}
	
	public void testUpdate()throws Exception{
		BoardVO board=new BoardVO();
		board.setBno(5);
		board.setTitle("수정된 글");
		board.setContent("소정된 내용");
		dao.update(board);
	}
	
	public void testDelete()throws Exception{
		dao.delete(5);
	}
	

}
