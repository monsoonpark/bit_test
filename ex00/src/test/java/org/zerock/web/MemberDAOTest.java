package org.zerock.web;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.zerock.domain.MemberVO;
import org.zerock.persistence.MemberDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/**/root-context.xml"})
public class MemberDAOTest {

	@Inject
	private MemberDAO dao;
	
	@Test
	public void testTime()throws Exception{
		System.out.println(dao.getTime());
	}
	
	public void testInsertMember()throws Exception{
		MemberVO vo= new MemberVO();
		vo.setUserid("user03");
		vo.setUserpw("user03");
		vo.setUsername("USER03");
		vo.setEmail("user00@aaa.com");
		dao.insertMember(vo);
	}
	
	@Test
	public void testSelect()throws Exception{
		System.out.println(dao.readMember("user00").getUserid());
		System.out.println(dao.readWithPW("user03", "user03").getUsername());
		
	}
	
	
}
