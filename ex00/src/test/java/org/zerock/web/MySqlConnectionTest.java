package org.zerock.web;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DriverManager;

import org.junit.Test;

public class MySqlConnectionTest {

	private static final String DRIVER="com.mysql.jdbc.Driver";
	private static final String URL="jdbc:mysql://127.0.0.1:3306/book_ex?useSSL=false&serverTimezone=UTC";
//	private static final String URL="jdbc:mysql://127.0.0.1:3306/book_ex?serverTimezone=Asia/Seoul";
	private static final String USER="zerock";
	private static final String PW="Tiger07#";
	
	
	@Test
	public void testConnection() throws Exception{
		Class.forName(DRIVER);
		try(Connection con=DriverManager.getConnection(URL, USER, PW)) {
			System.out.println(con);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void test() {
		fail("Not yet implemented");
	}

}
