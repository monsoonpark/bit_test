package org.zerock.web;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.zerock.domain.AmbVO;
import org.zerock.persistence.AmbDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/**/root-context.xml"})
public class AmbTest {

	@Inject
	private AmbDAO dao;
	
	@Test
	public void ambInsertTest(HttpServletRequest request)throws Exception{
		AmbVO vo=new AmbVO();
		vo.setTip(request.getRemoteAddr());
		vo.setTtitle("1번글");
		vo.setTwriter("1빠");
		vo.setTpw("1234");

		dao.ambInsert(vo);
	}
	
	@Test
	public void testSelect()throws Exception{
		for (AmbVO v: dao.ambSelect()) {
			System.out.println(v.getTnum());
		}
		
	}
	
	
}
