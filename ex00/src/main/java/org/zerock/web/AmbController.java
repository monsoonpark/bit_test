package org.zerock.web;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.zerock.domain.AmbVO;
import org.zerock.persistence.AmbDAO;

@Controller
public class AmbController {
	@Inject
	private AmbDAO dao;
	
	//회원정보 등록
	@RequestMapping(value="/amb_insert", method=RequestMethod.POST)
	public @ResponseBody HashMap<String, String> amb_insert(HttpServletRequest request, AmbVO vo) throws Exception {
		try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		//초기
		HashMap<String, String> states=new HashMap<>();
		states.put("state", "ok");
		System.out.println("amb_insert EXECUTE");
	
		//insert
		dao.ambInsert(vo);
		
		return states;
	}
	
	//회원목록
	@RequestMapping("/amb_list")
	public @ResponseBody List<AmbVO> memberList(Model model) throws Exception {
		List<AmbVO> list=dao.ambSelect();
		model.addAttribute(list);
		return list;
	}
	
}
