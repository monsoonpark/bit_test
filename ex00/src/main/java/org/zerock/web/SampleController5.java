package org.zerock.web;

import org.slf4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.zerock.domain.ProductVo;

@Controller
public class SampleController5 {
	private static final Logger logger=org.slf4j.LoggerFactory.getLogger(SampleController5.class);
	
	@RequestMapping("doJSON")
	public @ResponseBody ProductVo doJSON() {
		logger.info("doJSON called...");
		ProductVo vo=new ProductVo("샘플상품", 30000);
		
		return vo;
		
	}
}
