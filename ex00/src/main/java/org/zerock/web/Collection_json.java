package org.zerock.web;

import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.zerock.domain.ProductVo;

@Controller
public class Collection_json {
	
	@RequestMapping("/do_arraylist")
	public @ResponseBody ArrayList<ProductVo> doArraylistJson(){
		//초기
		ArrayList<ProductVo> ps=new ArrayList<>();
		ProductVo vo=null;
		//객체할당
		vo=new ProductVo("프라이스", 30000);
		ps.add(vo);
		vo=new ProductVo("치킨", 20000);
		ps.add(vo);
		vo=new ProductVo("마사져", 2000000);
		ps.add(vo);
		//리턴
		return ps;
	}
	
}
