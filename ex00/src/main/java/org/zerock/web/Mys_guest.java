package org.zerock.web;

public class Mys_guest {
	int gid;
	String gtitle;
	String gname;
	
	//
	public Mys_guest(int gid, String gtitle, String gname) {
		super();
		this.gid = gid;
		this.gtitle = gtitle;
		this.gname = gname;
	}
	public Mys_guest() {
		super();
	}
	
	//
	public int getGid() {
		return gid;
	}
	public void setGid(int gid) {
		this.gid = gid;
	}
	public String getGtitle() {
		return gtitle;
	}
	public void setGtitle(String gtitle) {
		this.gtitle = gtitle;
	}
	public String getGname() {
		return gname;
	}
	public void setGname(String gname) {
		this.gname = gname;
	}
	
	
}
