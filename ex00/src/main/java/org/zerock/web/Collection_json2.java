package org.zerock.web;

import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.zerock.domain.Jsonmember;
import org.zerock.domain.ProductVo;

@Controller
public class Collection_json2 {
	
	@RequestMapping("/do_arraylistjson")
	public @ResponseBody ArrayList<Jsonmember> doArraylistJson(){
		//초기
		ArrayList<Jsonmember> ms=new ArrayList<>();
		Jsonmember jm=null;
		//객체할당
		jm=new Jsonmember("홍길동", 27, "프로그래머");
		ms.add(jm);
		jm=new Jsonmember("이기훈", 25, "시스템엔지니어");
		ms.add(jm);
		jm=new Jsonmember("아이린", 28, "가수");
		ms.add(jm);
		
		//리턴
		return ms;
	}
	
}
