package org.zerock.web;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import org.zerock.domain.MemberVO;
import org.zerock.persistence.MemberDAO;

@Controller
public class MemberController {
	@Inject
	private MemberDAO dao;
	
	//
	@RequestMapping(value="/read", method= RequestMethod.GET)
	public void readMember(@RequestParam("userid") String userid, Model model) throws Exception {
		model.addAttribute(dao.read(userid));
	}
	
	@RequestMapping(value="/modify_member", method=RequestMethod.GET)
	public void modifyMemberForm(@RequestParam("userid") String userid, Model model) throws Exception {
		model.addAttribute(dao.read(userid));
	}
	@RequestMapping(value="/modify_member", method=RequestMethod.POST)
	public String modifyMember(HttpServletRequest request, MemberVO vo) throws UnsupportedEncodingException {
		
		System.out.println(request.getParameter("username"));
		System.out.println(vo.getUsername());
		dao.modifyMember(vo);
		return "redirect:member_list_ajax.jsp";
	}
	
	@RequestMapping(value="/remove_member", method=RequestMethod.POST)
	public String removeMember(@RequestParam("userid") String userid, RedirectAttributes rttr) {
		dao.removeMember(userid);
		return "redirect:member_list_ajax.jsp";
	}
	
	
	
	
	//회원정보 등록
	@RequestMapping(value="/member_insert_ajax", method=RequestMethod.POST)
	public @ResponseBody HashMap<String, String> insertMember_ajax(HttpServletRequest request, MemberVO vo) {
		try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		//초기
		HashMap<String, String> states=new HashMap<>();
		states.put("state", "ok");
		System.out.println("member_insert_ajax EXECUTE");
		//MemberVO vo=new MemberVO();
		//vo.setEmail(request.getParameter("email"));
		//vo.setUserid(request.getParameter("userid"));
		//vo.setUsername(request.getParameter("username"));
		//vo.setUserpw(request.getParameter("userpw"));
		
		//insert
		dao.insertMember(vo);
		
		return states;
	}
	
	//회원정보 등록
	@RequestMapping(value="/member_insert", method=RequestMethod.POST)
	public String insertMember(HttpServletRequest request) {
		try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		//초기
		MemberVO vo=new MemberVO();
		vo.setEmail(request.getParameter("email"));
		vo.setUserid(request.getParameter("userid"));
		vo.setUsername(request.getParameter("username"));
		vo.setUserpw(request.getParameter("userpw"));
		//insert
		dao.insertMember(vo);
		
		return "redirect:member_list";
	}
	
	
	//회원목록
	@RequestMapping("/member_list")
	public String memberList(Model model) throws Exception {
	
		model.addAttribute("list",dao.selectMembers());
		return "member_list";
	}
	//test 회원목록 JSON
	@RequestMapping("/member_list_ajax")
	public @ResponseBody List<MemberVO> memberListAjax(Model model) throws Exception{
		List<MemberVO> list=dao.selectMembers();
		model.addAttribute(list);
		return list; 
	}
	
	public String makeQuery(String userid) {
		UriComponentsBuilder uri=UriComponentsBuilder.newInstance();
		uri.path("read");
		uri.queryParam("userid", userid);
		uri.build();
		return uri.toUriString();
	}
	//회원목록 JSON: UriComponentsBuilder
	@RequestMapping("/member_list_ajax_ub")
	public @ResponseBody HashMap<String, MemberVO> memberListJson_ub( Model model) throws Exception{
		//초기
		HashMap<String, MemberVO> map=new HashMap<>();
		//Uri 추가
		List<MemberVO> ms=dao.selectMembers();
		for (MemberVO m : ms) {
			map.put(makeQuery(m.getUserid()), m);
		}
		
		return map; 
	}
	
}
