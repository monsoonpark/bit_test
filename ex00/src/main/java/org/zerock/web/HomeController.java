package org.zerock.web;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.zerock.domain.BigbirdsVO;
import org.zerock.domain.MemberM;
import org.zerock.mongos.MongoUtil;
import org.zerock.persistence.MemberDAO;
import org.zerock.persistence.MongoDAO;
import org.zerock.service.MongoService;

import com.google.common.collect.Lists;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Inject
	private MemberDAO dao;
	@Inject
	private MongoService mservice;
	@Inject
	private MongoDAO mdao;
	
	
	//합계 출력
	@RequestMapping(value="/count_testlogs", method=RequestMethod.GET)
	public String count(Model m) throws Exception {
		m.addAttribute("list", mservice.viewLogtest());
		return "count_testlogs";
	}
	
	//로그 데이터 입력
	@RequestMapping(value="/insert_testlogs", method=RequestMethod.GET)
	public @ResponseBody List<Document>  testLogs(HttpServletRequest request) throws Exception {
		//List<String> list=new ArrayList<>();
		MongoCollection<Document> col = MongoUtil.getCollection("test", "testlogs");
		List<Document> docs=new ArrayList<>();
		Document d=null;
		//SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// Document
		d=new Document();
		Calendar c=Calendar.getInstance();
		c.add(Calendar.HOUR_OF_DAY, 9);
		//d.append("date", sdf.parse("2018-06-25 03:20:01")).append("cate", 1);
		d.append("date", c.getTime()).append("cate", 1);
		docs.add(d);
		
		d=new Document();
		d.append("date", c.getTime()).append("cate", 2);
		docs.add(d);
		col.insertMany(docs);
		return docs;
	}
	
	//삭제
	@RequestMapping(value="/delete_bigbirds", method=RequestMethod.POST)
	public String deleteBigbirds(BigbirdsVO vo, Model m) throws Exception {
		m.addAttribute("list", mservice.delete(vo));
		return "update_bigbirds";
	}
	//상세 보기
	@RequestMapping(value="/update_bigbirds/{id}", method=RequestMethod.GET)
	public String detailBigbirds(@PathVariable("id")Integer id, Model m) throws Exception {
		m.addAttribute("list", mservice.listAll(id));
		return "detail_bigbirds";
	}
	//수정, 목록
	@RequestMapping(value="/update_bigbirds", method=RequestMethod.GET)
	public String updateBigbirdsPage(Model m) {
		  // return
		  try {
			 m.addAttribute("list", mservice.listAll());
		  } catch (Exception e) {
			  logger.error("예외! : " + e.getMessage());
		  }
		  // 
		  return "update_bigbirds";
	}
	@RequestMapping(value="/update_bigbirds", method=RequestMethod.POST)
	public String updateBigbirds( BigbirdsVO vo, Model m) throws Exception {
		m.addAttribute("list", mservice.modify(vo));
		return "update_bigbirds";
	}
	
	
	/* MongodbConnection List */
	@RequestMapping("/list_bigbirds_ajax")
	public @ResponseBody List<Document> listBigbirds_ajax(Model m) {
		// 컬렉션
		MongoCollection<Document> col = MongoUtil.getCollection("test", "bigbirds");
		// find
		FindIterable<Document> docs=col.find();
		//FindIterable을 Iterator로 변환
		Iterator<Document> it=docs.iterator();
		// Iterable을 list로 변환
		List<Document> list=Lists.newArrayList(it);
	
		// return
		return list;
	}
	
	/* MongodbConnection List */
	@RequestMapping("/list_bigbirds")
	public String listBigbirds(Model m) {
		// 초기
		//ArrayList<HashMap<String, Object>> list = new ArrayList<>();
		//HashMap<String, Object> hm=null;
		// 컬렉션
		MongoCollection<Document> col = MongoUtil.getCollection("test", "bigbirds");
		// find
		FindIterable<Document> docs=col.find();
		// Iterable을 list로 변환
		
		List<Document> list=new ArrayList<>();
		for(Document d: docs) {
			list.add(d);
		}
		m.addAttribute("list",list);
		
		// return
		return "list_bigbirds";
	}
	
	/* MongodbConnection */
	@RequestMapping("/insert_ajax_doc")
	public @ResponseBody List<String> insertAjaxDoc(@RequestBody MemberM m) {
		// 초기
		List<String> list = new ArrayList<>();
		// 컬렉션
		MongoCollection<Document> col = MongoUtil.getCollection("test", "insert_test");
		// Document
		Document doc = new Document("_id", m.get_id()).append("name", m.getName()).append("age", m.getAge())
				.append("job", m.getJob());
		// InsertOne
		col.insertOne(doc);
		// return
		list.add("OK");
		return list;
	}
	/*
	@RequestMapping(value="/insert_ajax_doc", method=RequestMethod.GET)
	public String insert_ajax_mongodb(Model m, HttpServletRequest request) {
		return "insert_ajax_mongodb";
	}
	@RequestMapping(value="/insert_ajax_doc", method=RequestMethod.POST)
	public @ResponseBody List<String> insert_ajax( HttpServletRequest request) {
		//초기
		List<String> list=new ArrayList<>();	
		int id=Integer.parseInt(request.getParameter("id"));
		String name=request.getParameter("name");
		int age=Integer.parseInt(request.getParameter("age"));
		String job=request.getParameter("job");
		
		//컬렉션
		MongoCollection<Document> col=MongoUtil.getCollection("test", "insert_test");
		//Document
		Document doc=new Document("_id",id).append("name", name).append("age", age).append("job", job);
		//InsertOne
		col.insertOne(doc);
		//return
		list.add("OK");
		
		return list;
	}
	*/
	/* MongodbConnection */
	@RequestMapping("/insert_document")
	public @ResponseBody List<String> insertDocument(Model m) {
		//초기
		List<String> list=new ArrayList<>();
		//컬렉션
		MongoCollection<Document> col=MongoUtil.getCollection("test", "insert_test");
		//Document
		Document doc=new Document("_id",2).append("name", "손나은").append("age", 29).append("job", "가수");
		//InsertOne
		col.insertOne(doc);
		//return
		list.add("OK");
		
		return list;
	}
	
	/* MongodbConnection */
	@RequestMapping("/mongoutil_test")
	public String testMongoutil(Model m) {
		
		MongoIterable<String> cols=MongoUtil.getDb("test").listCollectionNames();
		// 4. Iterable을 List로 변환
		List<String> list=new ArrayList<>();
		list=StreamSupport.stream(cols.spliterator(),false).collect(Collectors.toList());
		/*
		for(String name: cols) {
			list.add(name);
		}
		*/
		// 5. setAttribute
		m.addAttribute("list", list);
	
		return "mongoutil_test";
	}
	
	@RequestMapping("/mongo_test")
	public String testMongo(Model m) {
		// 0. Client 객체
		MongoClient mgc=new MongoClient("localhost", 27017);
		// 1. 연결
		MongoDatabase mgd=mgc.getDatabase("test");
		// 2. 연결 확인
		m.addAttribute("dbname", mgd.getName());
		
		/*
		for (String name : mgd.listCollectionNames()) {
			System.out.println(name);
		}
		*/
		mgc.close();
		return "mongo_test";
	}
	@RequestMapping("/list_collection")
	public String collection_list(Model m) {
		// 0. Client 객체
		MongoClient mgc=new MongoClient("localhost", 27017);
		// 1. 연결
		MongoDatabase mgd=mgc.getDatabase("test");
		// 2. 연결 확인
		m.addAttribute("dbname", mgd.getName());
		// 3. 컬렉션들
		MongoIterable<String> cols=mgd.listCollectionNames();
		List<String> list=new ArrayList<>();
		// 4. Iterable을 List로 변환
		list=StreamSupport.stream(cols.spliterator(),false).collect(Collectors.toList());
		/*
		for(String name: cols) {
			list.add(name);
		}
		*/
		// 5. setAttribute
		m.addAttribute("list", list);
		mgc.close();
		return "list_collection";
	}
	
	
	/* 네이버 프록시2 */
	@RequestMapping("/naver_proxy2")
	public ResponseEntity<String> doNaverProxy2(HttpServletRequest request) throws Exception {
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "application/json; charset=utf-8");

		String clientId = "93L6JwECD5a_lVRUw6bM";// 애플리케이션 클라이언트 아이디값";
		String clientSecret = "78lmZkdLpd";// 애플리케이션 클라이언트 시크릿값";
		// 매개변수 처리
		// 검색어
		String text = request.getParameter("text");
		text = URLEncoder.encode(Objects.isNull(text)||text.equals("") ? "올뉴그램" : text, "UTF-8");
		// 검색개수
		String display = request.getParameter("display");
		display = Objects.isNull(display) ? "10" : display;
		// 검색대상의 시작번호
		String start = request.getParameter("start");
		start = Objects.isNull(start) ? "1" : start;
		// 검색주제
		String target = request.getParameter("target");
		target = Objects.isNull(target) ? "news" : target;
		//
		System.out.println("text : " + text);
		System.out.println("target : " + target);

		String apiURL = String.format("https://openapi.naver.com/v1/search/%s?query=%s&display=%s&start=%s&sort=sim",
				target, text, display, start);
		// json 결과
		// String apiURL = "https://openapi.naver.com/v1/search/blog.xml?query="+ text;
		// // xml 결과
		URL url = new URL(apiURL);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("X-Naver-Client-Id", clientId);
		con.setRequestProperty("X-Naver-Client-Secret", clientSecret);
		int responseCode = con.getResponseCode();
		BufferedReader br;
		if (responseCode == 200) { // 정상 호출
			br = new BufferedReader(new InputStreamReader(con.getInputStream()));
		} else { // 에러 발생
			br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
		}
		String inputLine;
		StringBuffer res = new StringBuffer();
		while ((inputLine = br.readLine()) != null) {
			res.append(inputLine);
		}
		System.out.println("pre :" + res.toString());
		// json 출력
		return new ResponseEntity<String>(res.toString(), responseHeaders, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/naver_proxy", method=RequestMethod.GET, produces = "text/json; charset=UTF-8")
	public @ResponseBody Object doNaverProxy(HttpServletRequest request,Model model) throws Exception {
		String clientId = "VeyOPf3llkefDH8Nu0jb	";// 애플리케이션 클라이언트 아이디값";
		String clientSecret = "_aUHWNaPi7";// 애플리케이션 클라이언트 시크릿값";
		// 매개변수 처리
		String text = request.getParameter("text");
		text = URLEncoder.encode(Objects.isNull(text) || text.equals("") ? "스타워즈" : text, "UTF-8");
		String display = request.getParameter("display");
		display=Objects.isNull(display) || display.equals("") ? "10":display;
		String start = request.getParameter("start");
		start=Objects.isNull(start) || start.equals("") ? "1":start;
		String target = request.getParameter("target");
		target=Objects.isNull(target) || target.equals("") ? "news":target;
		
		System.out.println("text : " + text);
		System.out.println("target : " + target);

		String apiURL = String.format("https://openapi.naver.com/v1/search/%s?query=%s&display=%s&start=%s&sort=sim",
				target, text,  display, start); // json 결과
		// String apiURL = "https://openapi.naver.com/v1/search/blog.xml?query="+ text;
		// // xml 결과
		URL url = new URL(apiURL);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("X-Naver-Client-Id", clientId);
		con.setRequestProperty("X-Naver-Client-Secret", clientSecret);
		int responseCode = con.getResponseCode();
		BufferedReader br;
		if (responseCode == 200) { // 정상 호출
			br = new BufferedReader(new InputStreamReader(con.getInputStream()));
		} else { // 에러 발생
			br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
		}
		String inputLine;
		StringBuffer res = new StringBuffer();
		while ((inputLine = br.readLine()) != null) {
			res.append(inputLine);
		}
		System.out.println("pre :" + res.toString());
		model.addAttribute("list", res);
		return res;
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	
	@RequestMapping(value = "/test_arrlist", method = RequestMethod.GET)
	public String testArrList(Locale locale, Model model) {
		logger.info("Welcome test_arrlist! The client locale is {}.", locale);
					
		//ArrayList<Member> mems;
		//아이린(28,서울) 박보검(26,부산) 배수지(25,광주)
		
		ArrayList<Member> mems=new ArrayList<>();
		mems.add(new Member("아이린", 28, "서울"));
		mems.add(new Member("박보검", 26, "부산"));
		mems.add(new Member("배수지", 25, "광주"));
		model.addAttribute("mems", mems );
		
		return "test_arrlist";
	}
	
	@RequestMapping(value = "/mys_guest", method = RequestMethod.GET)
	public String listGuest(Locale locale, Model model) {
		logger.info("Welcome test_arrlist! The client locale is {}.", locale);
					
		//ArrayList<Mys_guest> guest=new ArrayList<>();
		ArrayList<Member> mems=new ArrayList<>();
		mems.add(new Member("아이린", 28, "서울"));
		mems.add(new Member("박보검", 26, "부산"));
		mems.add(new Member("배수지", 25, "광주"));
		model.addAttribute("guest", mems );
		
		return "listGuest";
	}
	
}
