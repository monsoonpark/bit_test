package org.zerock.web;

import org.slf4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.zerock.domain.Jsonmember;
import org.zerock.domain.ProductVo;

@Controller
public class TestJsonController {
	private static final Logger logger=org.slf4j.LoggerFactory.getLogger(TestJsonController.class);
	
	@RequestMapping("do_jsonmember")
	public @ResponseBody Jsonmember doJSON() {
		logger.info("do_jsonmember called...");
		
		return new Jsonmember("홍길동", 27, "프로그래머");
		
	}
}
