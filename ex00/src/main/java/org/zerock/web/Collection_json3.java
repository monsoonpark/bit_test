package org.zerock.web;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.zerock.domain.Jsonmember;
import org.zerock.domain.ProductVo;

@Controller
public class Collection_json3 {
	
	@RequestMapping("/do_map")
	public @ResponseBody HashMap<Integer , ProductVo> doHashMap(){
		//초기
		HashMap<Integer,ProductVo> hm=new HashMap<>();
		ProductVo vo=null;
		//객체할당
		vo=new ProductVo("프라이스", 30000);
		hm.put(1,vo);
		vo=new ProductVo("치킨", 20000);
		hm.put(2,vo);
		vo=new ProductVo("마사져", 2000000);
		hm.put(3,vo);
		//리턴
		return hm;
	}
	
}
