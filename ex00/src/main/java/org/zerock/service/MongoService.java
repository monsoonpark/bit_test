package org.zerock.service;

import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import org.bson.Document;
import org.springframework.stereotype.Service;
import org.zerock.domain.BigbirdsVO;
import org.zerock.persistence.MongoDAO;

import com.google.common.collect.Lists;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;

@Service
public class MongoService {

	@Inject
	private MongoDAO dao;

	public List<Document> viewLogtest() throws Exception {  
		// find  
		AggregateIterable<Document> docs = dao.selectTestlogs();  
		// FindIterable을 iterator로 변환  
		Iterator<Document> it = docs.iterator();  
		// iterator를 List<Document> 변환  
		List<Document> list = Lists.newArrayList(it);  
		return list; 
	}
	
	public List<Document> delete(BigbirdsVO vo) throws Exception{
		dao.deleteBirds(vo);
		return listAll();
	}
	
	public List<Document> modify(BigbirdsVO vo) throws Exception {
		dao.updateBirds(vo);
		return listAll();
	}
	
	public List<Document> listAll() throws Exception {
		// find
		FindIterable<Document> docs = dao.selectBigbirds();
		// FindIterable을 iterator로 변환
		Iterator<Document> it = docs.iterator();
		// iterator를 List<Document> 변환
		List<Document> list = Lists.newArrayList(it);
		return list;
	}
	public Document listAll(int id) throws Exception {
		// find
		Document docs = dao.selectBigbirds(id);
		// FindIterable을 iterator로 변환
		//Iterator<Document> it = docs.iterator();
		// iterator를 List<Document> 변환
		//List<Document> list = Lists.newArrayList(it);
		return docs;
	}
	
	
	
}
