package org.zerock.domain;

import java.util.Date;

public class AmbVO {
	private int tnum;
	private String ttitle;
	private String twriter;
	private String tpw;
	private Date tdate;
	private String tip;
	
	//
	public AmbVO(int tnum, String ttitle, String twriter, String tpw, Date tdate, String tip) {
		super();
		this.tnum = tnum;
		this.ttitle = ttitle;
		this.twriter = twriter;
		this.tpw = tpw;
		this.tdate = tdate;
		this.tip = tip;
	}
	public AmbVO() {
		super();
	}
	
	//
	public int getTnum() {
		return tnum;
	}
	public void setTnum(int tnum) {
		this.tnum = tnum;
	}
	public String getTtitle() {
		return ttitle;
	}
	public void setTtitle(String ttitle) {
		this.ttitle = ttitle;
	}
	public String getTwriter() {
		return twriter;
	}
	public void setTwriter(String twriter) {
		this.twriter = twriter;
	}
	public String getTpw() {
		return tpw;
	}
	public void setTpw(String tpw) {
		this.tpw = tpw;
	}
	public Date getTdate() {
		return tdate;
	}
	public void setTdate(Date tdate) {
		this.tdate = tdate;
	}
	public String getTip() {
		return tip;
	}
	public void setTip(String tip) {
		this.tip = tip;
	}
	
	
}
