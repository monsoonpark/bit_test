package org.zerock.domain;

public class MemberM {
	// 멤버변수
	private int _id;
	private String name;
	public int age;
	public String job;

	// 생성자
	public MemberM() {
	}

	public MemberM(int _id, String name, int age, String job) {
		super();
		this._id = _id;
		this.name = name;
		this.age = age;
		this.job = job;
	}

	// getter / setter
	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

}
