package org.zerock.domain;

public class BigbirdsVO {
	// 멤버변수
	 private int id;
	 private String name;
	 private int[] size;
	 private String anoname;
	 // 생성자
	 public BigbirdsVO() {}
	 public BigbirdsVO(int id, String name, int[] size, String anoname) {
	  super();
	  this.id = id;
	  this.name = name;
	  this.size = size;
	  this.anoname = anoname;
	 }
	 // getter/setter
	 public int getId() {
	  return id;
	 }
	 public void setId(int id) {
	  this.id = id;
	 }
	 public String getName() {
	  return name;
	 }
	 public void setName(String name) {
	  this.name = name;
	 }
	 public int[] getSize() {
	  return size;
	 }
	 public void setSize(int[] size) {
	  this.size = size;
	 }
	 public String getAnoname() {
	  return anoname;
	 }
	 public void setAnoname(String anoname) {
	  this.anoname = anoname;
	 }
	
	
}
