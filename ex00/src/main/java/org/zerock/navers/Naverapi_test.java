package org.zerock.navers;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Objects;

public class Naverapi_test {
	
	public static void main(String[] args) throws Exception {
		String clientId = "VeyOPf3llkefDH8Nu0jb";// 애플리케이션 클라이언트 아이디값";
		String clientSecret = "_aUHWNaPi7";// 애플리케이션 클라이언트 시크릿값";
		// 매개변수 처리
		String text = null;
		text = URLEncoder.encode(Objects.isNull(text) ? "스타워즈" : text, "UTF-8");
		String display = "10";
		String start = "1";
		String target = "news";
		System.out.println("text : " + text);
		System.out.println("target : " + target);

		String apiURL = String.format("https://openapi.naver.com/v1/search/%s?query=%s&display=%s&start=%s&sort=sim",
				Objects.isNull(target) ? "movie" : target, text, Objects.isNull(display) ? "2" : display,
				Objects.isNull(start) ? "1" : start); // json 결과
		// String apiURL = "https://openapi.naver.com/v1/search/blog.xml?query="+ text;
		// // xml 결과
		URL url = new URL(apiURL);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("X-Naver-Client-Id", clientId);
		con.setRequestProperty("X-Naver-Client-Secret", clientSecret);
		int responseCode = con.getResponseCode();
		BufferedReader br;
		if (responseCode == 200) { // 정상 호출
			br = new BufferedReader(new InputStreamReader(con.getInputStream()));
		} else { // 에러 발생
			br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
		}
		String inputLine;
		StringBuffer res = new StringBuffer();
		while ((inputLine = br.readLine()) != null) {
			res.append(inputLine);
		}
		System.out.println("pre :" + res.toString());

	}

}
