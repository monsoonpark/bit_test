package org.zerock.persistence;

import java.util.List;

import org.zerock.domain.AmbVO;

public interface AmbDAO {
	
	public void ambInsert(AmbVO vo);
	public List<AmbVO> ambSelect()throws Exception;
	
}
