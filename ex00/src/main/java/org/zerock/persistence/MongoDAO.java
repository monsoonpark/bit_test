package org.zerock.persistence;

import java.util.Arrays;
import java.util.List;

import org.bson.Document;
import org.springframework.stereotype.Repository;
import org.zerock.domain.BigbirdsVO;
import org.zerock.mongos.MongoUtil;

import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;

@Repository
public class MongoDAO {
	// 로그 가져오기
	public AggregateIterable<Document> selectTestlogs() {
		// 컬렉션
		MongoCollection<Document> col = MongoUtil.getCollection("test", "testlogs");
		// group
		Document firstGroup = new Document("$group",
			new Document("_id", new Document("month", new Document("$month", "$date"))
			.append("day", new Document("$dayOfMonth", "$date"))
			.append("year", new Document("$year", "$date")).append("hour", new Document("$hour", "$date")))
			.append("count", new Document("$sum", 1)));

		// 정렬
		Document sort = new Document("$sort", new Document("_id.hour", 1));
		List<Document> pipeline = Arrays.asList(firstGroup, sort);

		// return
		return col.aggregate(pipeline);
	}

	// Bigbirds SELECT
	public FindIterable<Document> selectBigbirds() {
		// 컬렉션
		MongoCollection<Document> col = MongoUtil.getCollection("test", "bigbirds");
		// return
		return col.find();
	}
	public Document selectBigbirds(int id) {
		// 컬렉션
		MongoCollection<Document> col = MongoUtil.getCollection("test", "bigbirds");
		// return
		return col.find(Filters.eq("_id", id)).first();
	}
	
	public void updateBirds(BigbirdsVO vo) {
		MongoCollection<Document> col = MongoUtil.getCollection("test", "bigbirds");
		col.updateOne(Filters.eq("_id", vo.getId()), Updates.set("anoname", vo.getAnoname()));
	}
	
	public void deleteBirds(BigbirdsVO vo) {
		MongoCollection<Document> col = MongoUtil.getCollection("test", "bigbirds");
		col.deleteOne(Filters.eq("_id", vo.getId()));
	}


	
}
