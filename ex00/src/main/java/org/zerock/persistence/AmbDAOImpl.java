package org.zerock.persistence;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;
import org.zerock.domain.AmbVO;

@Repository
public class AmbDAOImpl implements AmbDAO{

	@Inject
	private SqlSession sqlSession;
	
	private static final String namespace="org.zerock.mapper.AmbMapper";	
	
	@Override
	public void ambInsert(AmbVO vo) {
		sqlSession.insert(namespace+".ambInsert", vo);
	}

	@Override
	public List<AmbVO> ambSelect() throws Exception {
		return sqlSession.selectList(namespace+".ambSelect");
	}

}
