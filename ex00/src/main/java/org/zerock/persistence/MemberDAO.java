package org.zerock.persistence;

import java.util.List;

import org.zerock.domain.MemberVO;

public interface MemberDAO {
	
	public String getTime();
	
	public void insertMember(MemberVO vo);
	
	public MemberVO readMember(String userid)throws Exception;
	
	public MemberVO readWithPW(String userid,String userpw)throws Exception;
	
	public List<MemberVO> selectMembers() throws Exception;
	
	public MemberVO read(String userid)throws Exception;
	
	public void modifyMember(MemberVO vo);
	
	public void removeMember(String userid);
}
