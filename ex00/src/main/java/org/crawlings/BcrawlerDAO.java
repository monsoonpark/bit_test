package org.crawlings;

import java.util.List;

import org.bson.Document;
import org.springframework.stereotype.Repository;
import org.zerock.mongos.MongoUtil;

import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;

@Repository
public class BcrawlerDAO {
	
	
	//원하는 값만
	public FindIterable<Document> selectNewsKeywordsChart() {
		// 컬렉션
		MongoCollection<Document> col = MongoUtil.getCollection("test", "newskeywords");
		// return
		return col.find().projection(BasicDBObject.parse("{_id:0, id:1, count:1 }"));
	}
	//top10
	public FindIterable<Document> topSelect() {
		// 컬렉션
		MongoCollection<Document> col = MongoUtil.getCollection("test", "newskeywords");
		// return
		return col.find().projection(BasicDBObject.parse("{_id:0, id:1, count:1 }")).sort(Filters.eq("count", -1)).limit(7);
		
	}
	
	//컬렉션: newskeywords { id: 키워드, count: 회수, date: 조회일}
	public void insertNewsKeywords(List<NewsKeyword> news ) {
		MongoCollection<Document> col = MongoUtil.getCollection("test", "newskeywords");
		
		col.insertMany(news);
	}
	
	// Bcrawler insert
	public void insertBcrawler(Bcrawler c) {
		MongoCollection<Document> col = MongoUtil.getCollection("test", "crawlist");
		//List<Document> docs=new ArrayList<>();
		
		// Document	crawl
		//org.jsoup.nodes.Document d1 = getDoc(c.getUrl());
		//Elements name=d1.select(".newsnow_tx_inner a strong");
		//String url=d1.select(".newsnow_tx_inner a").attr("href");
		
		Document d=new Document();
		d.append("name", c.getName()).append("url", c.getUrl());
		
		col.insertOne(d);
	}
	
	// Bigbirds SELECT
	public FindIterable<Document> selectBcrawlers() {
		// 컬렉션
		MongoCollection<Document> col = MongoUtil.getCollection("test", "crawlist");
		// return
		return col.find();
	}
	
	public FindIterable<Document> selectCollection(String collection) {
		// 컬렉션
		MongoCollection<Document> col = MongoUtil.getCollection("test", collection);
		// return
		return col.find();
	}
	
	public FindIterable<Document> selectCollection(String collection, String url) {
		// 컬렉션
		MongoCollection<Document> col = MongoUtil.getCollection("test", collection);
		// return
		return col.find(Filters.eq("id", url) );
	}
	
	
	/*
	//URL을 전달받아 Document를 리턴
	public static org.jsoup.nodes.Document getDoc(String url) {
		//html 문서 객체
		org.jsoup.nodes.Document d=null;
		try {
			d = Jsoup.connect(url).get();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return d;
	}*/
}
