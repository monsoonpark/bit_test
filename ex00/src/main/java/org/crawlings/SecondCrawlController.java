package org.crawlings;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecondCrawlController {
	
	
	@RequestMapping(value="/img_crawl", method=RequestMethod.POST)
	public Map<String, String> ImgCrawl(@RequestBody String urls) {
		Elements emts = null;
		Map<String, String> imgs = new HashMap<>();
		String url = urls;
		//String text = null;
		String filename = null;
		String fullpath = null;
		String src = null;
		System.out.println(url);
		// parsing
		try {
			// img 태그들을 emts
			emts = Jsoup.connect(url).get().select("img");
			//
			for (Element e : emts) {
				// 경로에서 파일이름만 추출
				src = e.attr("src");
				filename = getWebFileName(src);
				// 기존에 존재하면 추가하지 않음
				fullpath = imgs.get(filename);
				if (Objects.isNull(fullpath)) {
					imgs.put(filename, src);
				}
				// 출력
				for (String key : imgs.keySet()) {
					System.out.printf("KEY : %s, VALUE : %s %n", key, imgs.get(key));
					takeImg(key, imgs.get(key));
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return imgs;
	}
		
	public static String getWebFileName(String filePath) {
		String[] parts = filePath.split("[/]");
		return parts[parts.length - 1];
	}
	
	public static void takeImg(String name, String src) {
		//초기
		String saveDir="C:\\sts-bundle\\images\\";
		URL url=null;
		String fullpath=src.substring(0,src.lastIndexOf(name));
		String filename=name.contains("?") ? name.substring(0,name.lastIndexOf("?")) : name ;
		System.out.println(filename+", "+fullpath);
		InputStream is=null;
			//파일 쓰기 스트림
		try(FileOutputStream fos=new FileOutputStream(saveDir+filename);
			) {
			
			url=new URL(fullpath+filename);
			byte[] buf=new byte[1024];
			int len=0;
			
			//파일 읽기 스트림
			is=url.openStream();
			//파일 읽기 / 쓰기
			while( (len=is.read(buf)) > 0 ) {
				fos.write(buf, 0, len);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(Objects.isNull(is)) {
				try {
					is.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}
}
