package org.crawlings;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class CrawlUtil {
	// 경로를 받아 파일이름만 리턴
	public static String getWebFileName(String filePath) {
		String[] parts = filePath.split("[/]");
		return parts[parts.length - 1];
	}

	// 저장폴더와 파일이름포함 전체경로를 받아 저장폴더에 파일들을 저장함
	public static void saveFiles(String saveDir, String fullpath) {
		// 초기
		// String saveDir = "C:\\sts-bundle\\images\\";
		URL url = null;
		//fullpath = fullpath.split("\\?")[0];
		
		// String fullpath =
		// "https://mimgnews.pstatic.net/image/origin/032/2018/06/27/2878598.jpg";
		String filename = getWebFileName(fullpath);

		// 파일쓰기 스트림
		InputStream is = null;
		try (FileOutputStream fos = new FileOutputStream(saveDir + filename)) {
			url = new URL(fullpath);
			byte[] buf = new byte[1024];
			int len = 0;

			// 파일읽기 스트림
			is = url.openStream();
			// 파일 읽기/쓰기
			while ((len = is.read(buf)) > 0) {
				fos.write(buf, 0, len);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (!Objects.isNull(is)) {
				try {
					is.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	// 페이지 주소를 전달하면 그 페이지의 이미지 경로와 파일이름 수집해서 Map<파일이름, 파일이름포함전체경로>으로 리턴
	public static Map<String, String> getImgUrl(String url) {
		// 초기
		Elements emts = null;
		Map<String, String> imgs = new HashMap<>();
		//String text = null;
		String filename = null;
		String fullpath = null;
		String src = null;
		// parsing
		try {
			// img 태그들을 emts
			emts = Jsoup.connect(url).get().select("img");
			//
			for (Element e : emts) {
				// 경로에서 파일이름만 추출
				src = e.attr("src").split("\\?")[0];
				filename = getWebFileName(src);
				// 기존에 존재하면 추가하지 않음
				fullpath = imgs.get(filename);
				if (Objects.isNull(fullpath)) {
					imgs.put(filename, src);
				}
				// 출력
				for (String key : imgs.keySet()) {
					System.out.printf("KEY : %s, VALUE : %s %n", key, imgs.get(key));
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// return
		return imgs;
	}
}
