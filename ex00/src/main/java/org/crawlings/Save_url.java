package org.crawlings;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Objects;

public class Save_url {
	
	public static void main(String[] args) {
		
		//초기
		String saveDir="C:\\sts-bundle\\images\\";
		URL url=null;
		String fullpath="https://imgnews.pstatic.net/image/news/2017/10/";
		String filename="snb_h_newsstand.png";
		
		
		InputStream is=null;
			//파일 쓰기 스트림
		try(FileOutputStream fos=new FileOutputStream(saveDir+filename);
			) {
			
			url=new URL(fullpath+filename);
			byte[] buf=new byte[1024];
			int len=0;
			
			//파일 읽기 스트림
			is=url.openStream();
			//파일 읽기 / 쓰기
			while( (len=is.read(buf)) > 0 ) {
				fos.write(buf, 0, len);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(Objects.isNull(is)) {
				try {
					is.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}
	
}
