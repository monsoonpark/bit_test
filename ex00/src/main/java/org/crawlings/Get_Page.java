package org.crawlings;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

public class Get_Page {
	
	public static void main(String[] args) {
		//변수선언
		String url="http://news.naver.com/";
		String output="";
		String line="";
		StringBuffer sb=new StringBuffer();
		
		// url읽어오기
		try( BufferedReader br=new BufferedReader(new InputStreamReader(new URL(url).openStream(), "UTF-8")) ) {
			//html을 String으로 parsing
			while( (line=br.readLine())!=null ) {
				sb.append(line);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Get_Page 에외: "+e.getMessage());
		}
		
		//출력
		System.out.println(sb.toString());
	}
}
