package org.crawlings;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Collect_img_url {

	public static String getWebFileName(String filePath) {
		String[] parts = filePath.split("[/]");
		return parts[parts.length - 1];
	}

	public static void main(String[] args) {
		/*
		 * 페이지 주소를 전달하면 그 페이지의 이미지 경로와 파일이름 수집해서 Map<파일이름, 파일이름포함전체경로>
		 * 
		 */

		// 초기
		Elements emts = null;
		Map<String, String> imgs = new HashMap<>();
		String url = "http://news.naver.com/";
		String text = null;
		String filename = null;
		String fullpath = null;
		String src = null;
		// parsing
		try {
			// img 태그들을 emts
			emts = Jsoup.connect(url).get().select("img");
			//
			for (Element e : emts) {
				// 경로에서 파일이름만 추출
				src = e.attr("src");
				filename = getWebFileName(src);
				// 기존에 존재하면 추가하지 않음
				fullpath = imgs.get(filename);
				if (Objects.isNull(fullpath)) {
					imgs.put(filename, src);
				}
				// 출력
				for (String key : imgs.keySet()) {
					System.out.printf("KEY : %s, VALUE : %s %n", key, imgs.get(key));
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
