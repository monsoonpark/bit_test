package org.crawlings;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.bson.Document;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.zerock.mongos.MongoUtil;

import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;

@RestController	//responsebody 없이 자동으로 json 리턴
public class BcrawlerContoller {

	private static final Logger logger=LoggerFactory.getLogger(BcrawlerContoller.class);
	
	@Inject
	private BcrawlerService service;
	
	//image download/* crawling_img ajax */
	@RequestMapping(value="/crawling_img", method=RequestMethod.POST)
	public List<String> crawlingImg(HttpServletRequest request ) {
		String pageurl=request.getParameter("pageurl");
		System.out.println(pageurl);
		String saveDir=request.getServletContext().getRealPath("imgs");
		List<String> list=new ArrayList<>();
		
		try {
			service.crawlingImages(saveDir + "\\", pageurl);
			list.add("OK");
		}catch(Exception e) {
			list.add("NO");
		}
		
		return list;
	}
	
	
	//키워드 리스트
	@RequestMapping(value="/list_crawling", method=RequestMethod.GET)
	public List<Document> listCrawling(Model m) throws Exception {
		//m.addAttribute("list", service.listAll());
		//return service.listAll();
		return service.listAll("newskeywords");
	}
	@RequestMapping(value="/list_keywords_chart", method=RequestMethod.GET)
	public List<Document> listKeywordsChart(Model m) throws Exception {
		//m.addAttribute("list", service.listAll());
		//return service.listAll();
		return service.listAllNewsKeywordsChart();
	}

	/* Keywords list ajax chart */
	@RequestMapping(value = "/list_keywords_charts", method = RequestMethod.GET)
	public Document listKeywordsChart() {
		// Document
		Document doc = new Document();
		List<BasicDBObject> cols = new ArrayList<>();
		List<BasicDBObject> rows = new ArrayList<>();
		doc.append("cols", cols);
		// cols
		cols.add(new BasicDBObject().append("label", "KEYWORD").append("type", "string"));
		cols.add(new BasicDBObject().append("label", "COUNT").append("type", "number"));
		// rows
		List<BasicDBObject> rowslist = new ArrayList<>();
		List<BasicDBObject> clist = null;
		// 0
		clist = new ArrayList<>();
		clist.add(new BasicDBObject().append("v", "KEY"));
		clist.add(new BasicDBObject().append("v", 3));
		rowslist.add(new BasicDBObject().append("c", clist));
		// 0
		clist = new ArrayList<>();
		clist.add(new BasicDBObject().append("v", "KEY"));
		clist.add(new BasicDBObject().append("v", 3));
		rowslist.add(new BasicDBObject().append("c", clist));
		// 0
		clist = new ArrayList<>();
		clist.add(new BasicDBObject().append("v", "KEY"));
		clist.add(new BasicDBObject().append("v", 3));
		rowslist.add(new BasicDBObject().append("c", clist));

		doc.append("rows", rowslist);

		// return
		try {
			return doc;
		} catch (Exception e) {
			logger.error("예외! : " + e.getMessage());
		}
		//
		//return new Document();
		return doc;

	}
	
	//top10
	@RequestMapping(value="/top_list", method=RequestMethod.GET)
	public List<Document> topList(Model m) throws Exception {
		//m.addAttribute("list", service.listAll());
		//return service.listAll();
		return service.listTop();
	}
	
	/* CRAWLING TEST */
	@RequestMapping(value = "/test_crawling", method = RequestMethod.GET)
	public List<Document> testCrawling() throws Exception {
		// 초기
		//List<String> list = null;
		
		// 제거 배열
		String[] stripChars = 
			{ ":", ";", ",", ".", "-", "_", "^", "~", "(", ")", "[", "]", "'", "?", "|", ">", "<","!", "\"", "{", "}", "/", "*", "&", "+", "$", "@", "%", "`", "#", "=", "·", "…", "’", "‘", "“", "”","→" };
		//
		// 컬렉션
		MongoCollection<Document> col = MongoUtil.getCollection("test", "crawlist");
		// find
		FindIterable<Document> docs = col.find();
		// crawlist의 각각의 주소를 크롤링
		String[] words = null;
		String text = null;
		Elements d = null;
		Map<String, Integer> counts = new HashMap<>();
		Matcher matcher = null;

		for (Document doc : docs) {
			// PAGE CRAWLING
			try {
				// 전체 HTML
				d = Jsoup.connect(doc.getString("url")).get().select("a[href]");
				// HTML을 TEXT로 파싱(태그 제거)
				text = Jsoup.parse(d.toString()).text();
				// logger.error("test1 : " + text);
				// 불필요한 기호 제거
				for (String s : stripChars) {
					// logger.error("- " + s);
					text = text.replace(s, " ");
				}
				// 로그로 확인
				logger.info("test2 : " + text);

				// HashMap에 단어별 빈도수
				// 뉴스, 3
				// 사회, 2
				words = text.split(" ");
				// 빈도수 출력
				Integer count = 1;
				String strTemp = null;
				// 1개이상의 수에 대한 패턴
				final Pattern pattern = Pattern.compile("\\d+", Pattern.MULTILINE); // \\d+ : 수1개이상...
				for (String s : words) {
					// 키워드가 수인경우 제외
					matcher = pattern.matcher(s);
					strTemp = matcher.replaceAll("");
					if (strTemp.equals("")) {
						continue;
					}
					// 새로 추가하는 경우 1, 기존인 경우 기+1
					count = counts.get(s);
					if (!Objects.isNull(count)) {
						count += 1;
					} else {
						count = 1;
					}
					// UPDATE
					counts.put(s, count);
				}
			} catch (Exception e) {
				logger.error("예외! : " + e.getMessage());
			}
		}
		// MAP 출력 / List로 변환
		Calendar c = Calendar.getInstance();
		c.add(Calendar.HOUR_OF_DAY, 9);
		List<NewsKeyword> news = new ArrayList<>();
		NewsKeyword n = null;
		for (String key : counts.keySet()) {
			n = new NewsKeyword();
			n.append("id", key);
			n.append("count", counts.get(key));
			n.append("date", c.getTime());
			news.add(n);
			System.out.println(key + " : " + counts.get(key));
			
		}
		// 키워드를 등록
		try {
			service.insert(news);
		} catch (Exception e) {
			logger.error("예외! : " + e.getMessage());
		}
		
		// 목록을 json으로 출력
		return service.listAll("newskeywords");
	}
	
	/* CRAWLING TEST */
	@RequestMapping(value = "/test_crawl", method = RequestMethod.GET)
	public List<Document> testCrawl(@RequestBody String url) throws Exception {
		// 초기
		//List<String> list = null;

		// PAGE CRAWLING
		try {
			// 전체 HTML
			Elements d = Jsoup.connect("http://news.naver.com/").get().select("a[href]");
			// HTML을 TEXT로 파싱(태그 제거)
			String text = Jsoup.parse(d.toString()).text();
			// logger.error("test1 : " + text);
			// 제거 배열
			String[] stripChars = { ":", ";", ",", ".", "-", "_", "^", "~", "(", ")", "[", "]", "'", "?", "|", ">", "<",
					"!", "\"", "{", "}", "/", "*", "&", "+", "$", "@", "%", "`", "#", "=", "·", "…", "’", "‘", "“", "”",
					"→" };
			// 불필요한 기호 제거
			for (String s : stripChars) {
				// logger.error("- " + s);
				text = text.replace(s, " ");
			}

			// 로그로 확인
			logger.error("test2 : " + text);

			// HashMap에 단어별 빈도수
			// 뉴스, 3
			// 사회, 2
			String[] words = text.split(" ");
			Map<String, Integer> counts = new HashMap<>();

			// 빈도수 출력
			Integer count = 1;
			String strTemp = null;
			// 1개이상의 수에 대한 패턴
			final Pattern pattern = Pattern.compile("\\d+", Pattern.MULTILINE);
			Matcher matcher = null;
			for (String s : words) {
				// 키워드가 수인경우 제외
				matcher = pattern.matcher(s);
				strTemp = matcher.replaceAll("");
				if (strTemp.equals("")) {
					continue;
				}
				// 새로 추가하는 경우 1, 기존인 경우 기+1
				count = counts.get(s);
				if (!Objects.isNull(count)) {
					count += 1;
				} else {
					count = 1;
				}
				// UPDATE
				counts.put(s, count);
			}
			// MAP 출력 / List로 변환
			Calendar c = Calendar.getInstance();
			c.add(Calendar.HOUR_OF_DAY, 9);
			List<Document> news = new ArrayList<>();
			Document n=new Document();
			for (String key : counts.keySet()) {
				n = new NewsKeyword();
				n.append("id", key);
				n.append("count", counts.get(key));
				n.append("date", c.getTime());
				n.append("date", c.getTime());
				news.add(n);
				System.out.println(key + " : " + counts.get(key));
				
			}
			// 키워드를 등록
			try {
				//service.insert(news);
			} catch (Exception e) {
				logger.error("예외! : " + e.getMessage());
			}

		} catch (Exception e) {
			logger.error("예외! : " + e.getMessage());
		}

		//
		return service.listAll("pagekeyword",url);
		
	}
	
	
	@RequestMapping(value="/insertUrl", method=RequestMethod.POST)
	public List<String> insertUrl(@RequestBody Bcrawler c) {
		List<String> list = new ArrayList<>();
		System.out.println(c.toString());
		try {
			service.insert(c);
			list.add("ok");
		} catch (Exception e) {
			list.add("error : " + e.getMessage());
		}
		return list;
	}
	
	@RequestMapping(value="/listCrawler", method=RequestMethod.GET)
	public List<Document> listCrawler(Model m) throws Exception {
		//m.addAttribute("list", service.listAll());
		//return service.listAll();
		return service.listAll();
	}
	
	
}
