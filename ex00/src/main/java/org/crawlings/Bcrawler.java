package org.crawlings;

public class Bcrawler {
	String name;
	String url;
	
	//
	public Bcrawler(String name, String url) {
		super();
		this.name = name;
		this.url = url;
	}
	public Bcrawler() {
		super();
	}
	
	//
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	//
	@Override
	public String toString() {
		return "Bcrawler [name=" + name + ", url=" + url + "]";
	}
	
	
	
	
	
	
}
