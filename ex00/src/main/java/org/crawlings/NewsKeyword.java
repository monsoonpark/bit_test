package org.crawlings;

import java.util.Date;

import org.bson.Document;

public class NewsKeyword extends Document {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//멤버변수
	
	String id;
	int	count;
	Date date;
	
	//
	public NewsKeyword() {
		super();
	}
	public NewsKeyword(String id, int count, Date date) {
		super();
		this.id = id;
		this.count = count;
		this.date = date;
	}
	
	//
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	//
	@Override
	public String toString() {
		return "NewsKeyword [id=" + id + ", count=" + count + ", date=" + date + "]";
	}

	
	
}
