package org.crawlings;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.bson.Document;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.mongodb.client.FindIterable;

@Service
public class BcrawlerService {

	@Inject
	private BcrawlerDAO dao;
		
	// 페이지 URL과 저장폴더를 받아서 image 크롤링
	public void crawlingImages(String saveDir, String pageurl) {
		// 파일이름/경로 Map
		Map<String, String> maps = CrawlUtil.getImgUrl(pageurl);
		// 진행률
		int totalsize = maps.size();
		int count = 1;
		System.out.println("== DOWNLOADING...");
		for (String key : maps.keySet()) {
			CrawlUtil.saveFiles(saveDir, maps.get(key));
			System.out.printf("%s %% ( %s)ING... %n", count * 100 / totalsize, key);
			count++;
		}
		System.out.println("== DOWNLOADED");
	}
	
	
	//키워즈 등록
	public void insert(List<NewsKeyword> vo) throws Exception {
		dao.insertNewsKeywords(vo);
	}
	
	//Crawlinglist 등록
	public void insert(Bcrawler vo) throws Exception {
		dao.insertBcrawler(vo);
	}

	public List<Document> listTop() throws Exception {
		// find
		FindIterable<Document> docs = dao.topSelect();
		// FindIterable을 iterator로 변환
		Iterator<Document> it = docs.iterator();
		// iterator를 List<Document> 변환
		List<Document> list = Lists.newArrayList(it);
		return list;
	}
	
	public List<Document> listAllNewsKeywordsChart() throws Exception {
		// find
		FindIterable<Document> docs = dao.selectNewsKeywordsChart();
		// FindIterable을 iterator로 변환
		Iterator<Document> it = docs.iterator();
		// iterator를 List<Document> 변환
		List<Document> list = Lists.newArrayList(it);
		return list;
	}
	public List<Document> listAll() throws Exception {
		// find
		FindIterable<Document> docs = dao.selectBcrawlers();
		// FindIterable을 iterator로 변환
		Iterator<Document> it = docs.iterator();
		// iterator를 List<Document> 변환
		List<Document> list = Lists.newArrayList(it);
		return list;
	}
	
	public List<Document> listAll(String collection) throws Exception {
		// find
		FindIterable<Document> docs = dao.selectCollection(collection);
		// FindIterable을 iterator로 변환
		Iterator<Document> it = docs.iterator();
		// iterator를 List<Document> 변환
		List<Document> list = Lists.newArrayList(it);
		return list;
	}
	
	public List<Document> listAll(String collection, String url) {
		// find
		FindIterable<Document> docs = dao.selectCollection(collection, url);
		// FindIterable을 iterator로 변환
		Iterator<Document> it = docs.iterator();
		// iterator를 List<Document> 변환
		List<Document> list = Lists.newArrayList(it);
		return list;
	}
	
}
