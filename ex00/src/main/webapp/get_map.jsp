<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>get_arraylist.jsp</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	$(function(){
		$.get("do_map",function(data, state){
			if(state=="success"){
				//for ( var d in data) {
				//	$("#ulMap").append("<li>"+data[d].name+"("+data[d].price+")</li>");
				//}
				
				/*
				Object.keys(객체)	//객체의 키를 get
				.forEach(function(k){	//callback 함수 자동으로 인수값(k) 적용
					alert(data[k].name);
				});
				*/
				//alert(Object.keys(data).length);
				Object.keys(data).forEach(function(k){
					$("#ulMap tbody").append("<tr><td>"+data[k].name+"</td><td>"+data[k].price+"</td></tr>");
				});
			}else{
				alert("실패");
			}
		});
	});	
</script>
</head>
<body>
	<h1>HashMap</h1>
	<table id="ulMap" border="1">
		<thead>
			<tr>
				<td>이름</td><td>가격</td>
			</tr>
		</thead>
		<tbody>
			
		</tbody>
	</table>
</body>
</html>