<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>get_json.jsp</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	$(function(){
		/*
		$.get("doJSON",function(data, state){
			GET한 경우 자동실행되는 명령
		});
		*/
		$.get("doJSON",function(data, state){
			//alert(state);
			if(state=="success"){
				//성공한 경우
				alert("GET 성공");
				//alert(data.name);
				$("#ulGift").append("<li>"+data.name+"</li>");
				$("#ulGift").append("<li>"+data.price+"</li>");
			}else{
				//실패한 경우
				alert("GET 실패!");
			}
		});
	});	
</script>
</head>
<body>
	<h1>doJSON에게 받은 선물</h1>
	<ul id="ulGift">
		
	</ul>
</body>
</html>