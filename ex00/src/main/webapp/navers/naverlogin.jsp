<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>naverlogin.jsp</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://static.nid.naver.com/js/naverLogin_implicit-1.0.3-min.js"></script>
<script>
	/* 네이버 로그아웃 함수 */
	function logout() {
		// 로그아웃 아이프레임
		$("body").append(
				"<iframe id='logoutIframe' style='display: none;'></iframe>");
		$("#logoutIframe").attr("src", "http://nid.naver.com/nidlogin.logout");
		// 로그아웃 처리
		$("#naver_id_login").show();
		$("#naver_id_logout").hide();
	}
</script>
</head>
<body>
	<h1>NAVER LOGIN :</h1>
	<!-- 로그인 안 한 경우  -->
	<div id="naver_id_login"></div>
	<!-- 로그인 한 경우 -->
	<div id="naver_id_logout" style="display: none;">
		<a href="#" onclick="logout();">로그아웃</a>
		<div id="dvLogin"></div>
	</div>

	<!-- 네이버아디디로로그인 초기화 Script -->
	<script type="text/javascript">
		var naver_id_login = new naver_id_login("VeyOPf3llkefDH8Nu0jb",
				"http://192.168.0.61:8080/web/navers/naverloginpop.jsp");
		var state = naver_id_login.getUniqState();
		naver_id_login.setButton("white", 2, 40);
		naver_id_login.setDomain("192.168.0.61:8080/");
		naver_id_login.setState(state);
		naver_id_login.setPopup();
		naver_id_login.init_naver_id_login();
	</script>

</body>
</html>