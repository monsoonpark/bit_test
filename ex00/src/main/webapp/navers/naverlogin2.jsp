<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>naverlogin2.jsp</title>
<!-- Bootstrap -->
<link rel="stylesheet"
 href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
 integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
 crossorigin="anonymous">

</head>
<body>
	<div class="header clearfix">
		<nav>
			<ul class="nav nav-pills pull-right">
				<li role="presentation" class="active"><a href="#">Home</a></li>
				<li role="presentation"><a id="gnbLogin" href="#">Login</a></li>
			</ul>
		</nav>
	</div>
	<div id="naverIdLogin">
		<a id="naverIdLogin_loginButton" href="#" role="button"><img
			src="https://static.nid.naver.com/oauth/big_g.PNG" width=320></a>
	</div>
	<!-- /container -->
 <script src="https://code.jquery.com/jquery-1.12.1.min.js"></script>
 <script
  src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

 <!-- (2) LoginWithNaverId Javscript SDK -->
 <script
  src="http://static.nid.naver.com/js/naveridlogin_js_sdk_2.0.0.js"></script>

 <!-- (3) LoginWithNaverId Javscript 설정 정보 및 초기화 -->
 <script>
  var naverLogin = new naver.LoginWithNaverId({
   clientId : "VeyOPf3llkefDH8Nu0jb",
   callbackUrl : "http://192.168.0.61:8080/web/navers/naverloginpop.jsp",
   isPopup : false,	
   loginButton : {
    color : "green",
    type : 3,
    height : 60
   }
  });
  /* (4) 네아로 로그인 정보를 초기화하기 위하여 init을 호출 */
  naverLogin.init();

  /* (4-1) 임의의 링크를 설정해줄 필요가 있는 경우 */
  $("#gnbLogin").attr("href", naverLogin.generateAuthorizeUrl());

  /* (5) 현재 로그인 상태를 확인 */
  window.addEventListener('load', function() {
   naverLogin.getLoginStatus(function(status) {
    if (status) {
     /* (6) 로그인 상태가 "true" 인 경우 로그인 버튼을 없애고 사용자 정보를 출력합니다. */
     setLoginStatus();
    }
   });
  });

  /* (6) 로그인 상태가 "true" 인 경우 로그인 버튼을 없애고 사용자 정보를 출력합니다. */
  function setLoginStatus() {
   var profileImage = naverLogin.user.getProfileImage();
   var nickName = naverLogin.user.getNickName();
   $("#naverIdLogin_loginButton").html(
     '<br><br><img src="' + profileImage + '" height=50 /> <p>'
       + nickName + '님 반갑습니다.</p>');
   $("#gnbLogin").html("Logout");
   $("#gnbLogin").attr("href", "#");
   /* (7) 로그아웃 버튼을 설정하고 동작을 정의합니다. */
   $("#gnbLogin").click(function() {
    naverLogin.logout();
    location.reload();
   });
  }
 </script>

</body>
</html>