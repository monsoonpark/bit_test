<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>naverlogin.jsp</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://static.nid.naver.com/js/naverLogin_implicit-1.0.3-min.js"></script>
</head>
<body>
	<h1>NAVER LOGIN POP:</h1>
	<!-- 로그인 안 한 경우  -->
	<!-- 네이버아이디로로그인 버튼 노출 영역 -->
	<div id="naver_id_login" style="display: none;"></div>
	<div id="naver_id_logout" style="display: none;">
		<ul></ul>
		<a href="javascript:window.close();">창닫기</a>
	</div>
	<!-- 네이버아디디로로그인 초기화 Script -->
	<script type="text/javascript">
		var naver_id_login = new naver_id_login("VeyOPf3llkefDH8Nu0jb",
				"http://192.168.0.61:8080/web/navers/naverloginpop.jsp");
		var state = naver_id_login.getUniqState();
		naver_id_login.setButton("white", 2, 40);
		naver_id_login.setDomain("192.168.0.120:8080/");
		naver_id_login.setState(state);
		naver_id_login.setPopup();
		naver_id_login.init_naver_id_login();
	</script>
	<!-- // 네이버아이디로로그인 초기화 Script -->
	<!-- 네이버아디디로로그인 Callback페이지 처리 Script -->
	<script type="text/javascript">
		// 네이버 사용자 프로필 조회 이후 프로필 정보를 처리할 callback function
		function naverSignInCallback() {
			// 네이버 사용자 프로필 조회
			// 로그인 처리
			$("#naver_id_logout").show();
			$(opener.document).find("#naver_id_login").hide();
			$(opener.document).find("#naver_id_logout").show();
			// 창닫기 및 로그인입력
			var ul = $("#naver_id_logout ul");
			ul.empty();
			ul.append("<li>nickname : "
					+ naver_id_login.getProfileData('nickname') + "</li>"
					+ "<li>age : " + naver_id_login.getProfileData('age')
					+ "</li><li>age : " + naver_id_login.getProfileData('email')
					+ "</li>");
		}
		// 네이버 사용자 프로필 조회
		naver_id_login.get_naver_userprofile("naverSignInCallback()");
	</script>
	<!-- //네이버아디디로로그인 Callback페이지 처리 Script -->

</body>
</html>