<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>
<link href="/web/resources/css/mynaver.css" rel="stylesheet"
	type="text/css">
<script type="text/javascript">
	function listbind() {
		$("#ullist").empty();
		link = "naver_proxy2?text=" + $("#search").val() + "&target="
				+ $("#selSearch").val() + "&display=" + $("#selDisplay").val();
		$.get(link,function(data, state) {
			if (state == "success") {
				//$("#title").text(data.items[0].title);
				//$("#content").text(data.items[0].description);
				for ( var d in data.items) {
					var i = data.items[d];
					//alert(data.items[d].title);
					ul = $("#ullist");
					ul.append("<li><a href='"+i.link+"' target='_blank'><img src='"+
							i.image+"' title='"+i.title+"' alt='"+i.title+"'/>"
							+ "</a><br/>"+ i.title
							+ " ( "	
							+ addComma(i.lprice)+ " ~ "+ addComma(i.hprice)
							+ " )"+ "</li>");
					if (data.total != null) {
						$("#total").val(
								"( " + data.total + " )");
					}
				}
			}
		});
	}
	function addComma(num) {
		var regexp = /\B(?=(\d{3})+(?!\d))/g;
		return num.toString().replace(regexp, ',');
	}
	$(function() {
		//test
		$("#btnSearch").click(function() {
			//bindjson();
			listbind();

		});

	});
</script>
<style>
li {
	list-style: none;
}
img{
	width:400px;
	height:400px;
}
</style>
</head>
<body>

	<div class="container-full">
		<div class="row">
			<div class="col-lg-12 text-center v-center">
				<h1>
					<a href="">Naver Shop</a>
				</h1>
				<p class="lead">Test Page</p>

				<form class="col-lg-4">
					<div class="form-inline input-group-lg col-sm-offset-12 col-sm-4">
						<span class="input-group-btn"> <select id="selSearch"
							class="form-control">
								<option value="news">뉴스</option>
								<option value="movie">영화</option>
								<option value="blog">블로그</option>
								<option value="book">책</option>
								<option value="shop" selected>쇼핑</option>
						</select> <select id="selDisplay" class="form-control">
								<option value="5">5</option>
								<option value="10" selected>10</option>
								<option value="20">20</option>
								<option value="40">40</option>
						</select>
						</span>
					</div>
				</form>
				<form class="col-lg-12">

					<div class="input-group input-group-lg col-sm-offset-4 col-sm-4">
						<span class="input-group-btn"> <input type="text"
							id="search" class="center-block form-control input-lg"
							title="Search" placeholder="Search">
							<button id="btnSearch" class="btn btn-lg btn-primary"
								type="button">검색</button>
						</span>
					</div>
				</form>
			</div>
		</div>
		<!-- /row -->

		<div class="row">
			<div class="col-lg-12 text-center v-center" style="font-size: 39pt;">
				<a href="#"><i class="icon-google-plus"></i></a> <a href="#"><i
					class="icon-facebook"></i></a> <a href="#"><i class="icon-twitter"></i></a>
				<a href="#"><i class="icon-github"></i></a> <a href="#"><i
					class="icon-pinterest"></i></a>
			</div>
		</div>

	</div>
	<!-- /container full -->

	<div class="container">

		<hr>

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3>
							naver_proxy2 <input id="total" value="" disabled />
						</h3>
					</div>
					<div class="panel-body">
						<ul id="ullist">

						</ul>
					</div>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-lg-12">
				<br>
				<p class="pull-right">
					<a href="http://localhost:8080/web/naver_shop2.jsp">Template
						from Bootply</a> &nbsp; ©Copyright 2013 ACME<sup>TM</sup> Brand.
				</p>
				<br> <br>
			</div>
		</div>
	</div>


</body>
</html>