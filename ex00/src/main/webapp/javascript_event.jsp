<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>javascript_event.jsp</title>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript">

	$(function(){
		$("a").click(function(e){
			//alert(e);
			e.preventDefault();
			alert("네이버면 안가요");
		});
			
	});

	function doClick(e){
		//alert("네이버면 안가요");
		e.preventDefault();
		alert("네이버면 안가요.");
	}
	
</script>
</head>
<body>
	<h1>preventDefault() : 기본액션 중지</h1>
	<a href="http://www.naver.com" target="_blank">네이버로 가기</a>
</body>
</html>