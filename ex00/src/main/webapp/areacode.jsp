<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>areacode.jsp</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">

	//성공함수
	function bindSido(data, state){
		//alert($(data).find("item").length);
		var items=$(data).find("item");
		var sd=$("#selSido");
		for (var i = 0; i < items.length; i++) {
			item=$(items[i]);
			//console.log(item.find("name").text());
			sd.append("<option value='"+item.find("code").text()+"'>"+item.find("name").text()+"</option>");
		}
	}
	function bindSelgu(data, state){
		//alert($(data).find("item").length);
		var items=$(data).find("item");
		var sg=$("#selGu");
		for (var i = 0; i < items.length; i++) {
			item=$(items[i]);
			//console.log(item.find("name").text());
			sg.append("<option value='"+item.find("code").text()+"'>"+item.find("name").text()+"</option>");
		}
	}
	function bindList(data, state){
		var items=$(data).find("item");
		var ul=$("#ulList");
		$("#ulList li").remove();
		for (var i = 0; i < items.length; i++) {
			item=$(items[i]);
			//console.log(item.find("name").text());
			ul.append("<li><a href='#"+item.find("contentid").text()+"' class='btn btn-info' data-toggle='collapse'>"
					+item.find("title").text()+"</a>"
					+"<div id='"+item.find("contentid").text()+"' class='collapse'>"
					+item.find("addr1").text()+item.find("addr2").text()+"<br/>"
					+"<img src='"
						+(item.find("firstimage2").text()==null?item.find("firstimage").text():item.find("firstimage2").text()) 
						+"'/></div>"
					+"</li>");
		}
	}
	
	$(function(){
		$.get("ca_areacode.jsp", bindSido);
		$("#selSido").on("change",function(){
			$("#selGu option").remove();
			//console.log($(this).val());
			$.get("ca_areacode.jsp?", {areaCode: $(this).val()}, bindSelgu);
		});
		$("#selGu").on("change",function(){
			//$.get("ca_areacode.jsp?areaCode="+$(this).val(), bindSelgu);
			//alert($("#selSido").val()+", "+$("#selGu").val());
		});
		
		$("#searchBtn").click(function(){
			$.get("script_tour.jsp", 
					{areaCode: $("#selSido").val(), sigunguCode: $("#selGu").val()}, 
					bindList);
		});
		
	});
</script>
</head>
<body>
	<h1>서울시 코드</h1>
	
	<ul>
		<li>
			시도 : <select id="selSido"></select>
		</li>
		<li>
			군구 : <select id="selGu"></select>
		</li>
		<li><button id="searchBtn">Search</button></li>
	</ul>
	
	<ul id="ulList">
		
	</ul>


</body>
</html>