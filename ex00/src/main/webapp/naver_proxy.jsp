<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page import="java.util.Objects"%>
<%@page import="java.net.URLEncoder"%>
<%@ page language="java" contentType="application/json; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%
	//"application/json; charset=UTF-8"
	String clientId = "VeyOPf3llkefDH8Nu0jb";// 애플리케이션 클라이언트 아이디값";
	String clientSecret = "_aUHWNaPi7";// 애플리케이션 클라이언트 시크릿값";
	// 매개변수 처리
	// 검색어
	String text = request.getParameter("text");
	text = URLEncoder.encode(Objects.isNull(text) ? "스타워즈" : text, "UTF-8");
	// 검색개수
	String display = request.getParameter("display");
	display = Objects.isNull(display) ? "10" :  display;
	// 검색대상의 시작번호
	String start = request.getParameter("start");
	start = Objects.isNull(start) ? "1" :  start;
	// 검색주제
	String target = request.getParameter("target");
	target = Objects.isNull(target) ? "news" :  target;
	// 
	System.out.println("text : " + text);
	System.out.println("target : " + target);

	String apiURL = String.format(
			"https://openapi.naver.com/v1/search/%s?query=%s&display=%s&start=%s&sort=sim",
			Objects.isNull(target) ? "movie" : target, text, Objects.isNull(display) ? "2" : display,
			Objects.isNull(start) ? "1" : start); // json 결과
	// String apiURL = "https://openapi.naver.com/v1/search/blog.xml?query="+ text;
	// // xml 결과
	URL url = new URL(apiURL);
	HttpURLConnection con = (HttpURLConnection) url.openConnection();
	con.setRequestMethod("GET");
	con.setRequestProperty("X-Naver-Client-Id", clientId);
	con.setRequestProperty("X-Naver-Client-Secret", clientSecret);
	int responseCode = con.getResponseCode();
	BufferedReader br;
	if (responseCode == 200) { // 정상 호출
		br = new BufferedReader(new InputStreamReader(con.getInputStream()));
	} else { // 에러 발생
		br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
	}
	String inputLine;
	StringBuffer res = new StringBuffer();
	while ((inputLine = br.readLine()) != null) {
		res.append(inputLine);
	}
	System.out.println("pre :" + res.toString());
	//json 출력
	out.print(res.toString());
%>