<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>script_commonapi.jsp</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">

	
	//var xhr = new XMLHttpRequest();
	var url = 'http://api.visitkorea.or.kr/openapi/service/rest/KorService/detailCommon'; /*URL*/
	var queryParams = '?' + encodeURIComponent('ServiceKey') + '='
			+'I81xROGGmr0CCOnnUpVUyd1qzQ2%2BvUeOOA02QHTrZVQtf8tCq4RAP02BBIwgaPfsUN8U5y7BvqnwPyGdgY%2B6zw%3D%3D'; /*Service Key*/
	//queryParams += '&' + encodeURIComponent('ServiceKey') + '=' + encodeURIComponent('인증키'); /*공공데이터포털에서*/
	queryParams += '&' + encodeURIComponent('numOfRows') + '=' + encodeURIComponent('100'); /*한 페이지 결과 수*/
	queryParams += '&' + encodeURIComponent('pageNo') + '=' + encodeURIComponent('1'); /*현재 페이지 번호*/
	queryParams += '&' + encodeURIComponent('MobileOS') + '=' + encodeURIComponent('ETC'); /*IOS(아이폰),AND(안드로이드),WIN(원도우폰),ETC*/
	queryParams += '&' + encodeURIComponent('MobileApp') + '=' + encodeURIComponent('AppTest'); /*서비스명=어플명*/
	queryParams += '&' + encodeURIComponent('contentId') + '=' + encodeURIComponent('126508'); /*콘텐츠ID*/
	queryParams += '&' + encodeURIComponent('contentTypeId') + '=' + encodeURIComponent('12'); /*관광타입(관광지, 숙박 등) ID*/
	queryParams += '&' + encodeURIComponent('defaultYN') + '=' + encodeURIComponent('Y'); /*기본정보 조회여부*/
	queryParams += '&' + encodeURIComponent('firstImageYN') + '=' + encodeURIComponent('Y'); /*원본, 썸네일 대표이미지 조회여부*/
	queryParams += '&' + encodeURIComponent('areacodeYN') + '=' + encodeURIComponent('Y'); /*지역코드, 시군구코드 조회여부*/
	queryParams += '&' + encodeURIComponent('catcodeYN') + '=' + encodeURIComponent('Y'); /*대,중,소분류코드 조회여부*/
	queryParams += '&' + encodeURIComponent('addrinfoYN') + '=' + encodeURIComponent('Y'); /*주소, 상세주소 조회여부*/
	queryParams += '&' + encodeURIComponent('mapinfoYN') + '=' + encodeURIComponent('Y'); /*좌표 X,Y 조회여부*/
	queryParams += '&' + encodeURIComponent('overviewYN') + '=' + encodeURIComponent('Y'); /*콘텐츠 개요 조회여부*/
	
	console.log(url + queryParams);
	/*
	xhr.open('GET', url + queryParams);
	xhr.onreadystatechange = function () {
	    if (this.readyState == 4) {
	        alert('Status: '+this.status+' Headers: '+JSON.stringify(this.getAllResponseHeaders())+' Body: '+this.responseText);
	    }
	};
	xhr.send('');
	*/
	//시작이벤트
	$(function(){
		//요청
		$.ajax({
			url: url + queryParams
			, timeout: 10000
			, type: 'get'
			, dataType: 'xml'
			, success: function(data){
				//alert(data);
				//ulView에 경복궁 데이터를 출력
				//alert($(data).find('title').text());
				$("#dvCtn > h1").append($(data).find("title").text());
				$("#ulView").append("<li>"+$(data).find("addr1").text()+$(data).find("addr2").text()+"</li>");
				$("#ulView").append("<li><img src='"+$(data).find("firstimage2").text()+"'/></li>");
				$("#ulView").append("<li>"+$(data).find("homepage").text()+"</li>");
				$("#ulView").append("<li>"+$(data).find("overview").text()+"</li>");
				
			}, error: function(error){
				alert(error);
			}
		});
	});
	
</script>
</head>
<body>
<div id="dvCtn">
	<h1></h1>
	
	<ul id="ulView">
		
	</ul>
</div>


</body>
</html>