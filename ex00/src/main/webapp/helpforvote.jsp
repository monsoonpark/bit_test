<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">
	function party(data, state){
		var items=$(data).find("item");
		var p=$("#partyList");
		for (var i = 0; i < items.length; i++) {
			item=$(items[i]);
			p.append("<option value='"+item.find("polyCd").text()+"'>"+item.find("polyNm").text()+"</option>")
		}
	}
	function votelocation(data,state){
		var items=$(data).find("item");
		var v=$("#votelocation");
		for (var i = 0; i < items.length; i++) {
			item=$(items[i]);
			v.append("<option value='"+item.find("origCd").text()+"'>"+item.find("origNm").text()+"</option>")
		}
	}
	function bindList(url){
		$.get(url, {poly_cd : $("#partyList").val(), orig_cd : $("#votelocation").val()  } , function(data, state){
			var items=$(data).find("item");
			var member=$("#partyMember");
			$("#partyMember li").remove();
			for (var i = 0; i < items.length; i++) {
				item=$(items[i]);
				member.append("<li><a href='#"+item.find("deptCd").text()+"' class='btn btn-info' data-toggle='collapse'>"
						+item.find("empNm").text()+"("+item.find("reeleGbnNm").text()+")</a>"
						+"<div id='"+item.find("deptCd").text()+"' class='collapse'>"
						+item.find("origNm").text()+"<br/>"
						+"<img src='"+ item.find("jpgLink").text()+"'/></div>"
						+"</li>");
			}
		})
	
	}
	$(function(){
		$.get("partylist.jsp", party);
		$.get("votelocation.jsp", votelocation);
		$("#partyList").on("change", function(){
			bindList("partymember.jsp");
		});
		
		$("#votelocation").on("change", function(){
			bindList("partymember2.jsp");
		});
		
		
	});
</script>
</head>
<body>
<h1>국회의원 명단<small> (wait for loading...)</small></h1>
<ul>
	<li>
		정당별: <select id="partyList">	
					<option value="">전체</option>
				</select>
	</li>
	<li>
		지역별: <select id="votelocation">
					<option value="">전체</option>
				</select>
	</li>
	
	<li>
		<ul id="partyMember">
		</ul>
	</li>
</ul>
</body>
</html>