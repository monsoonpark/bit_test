<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>html_position.jsp</title>
<style type="text/css">
	*{	padding:0px; margin:0px;	}
	/* 전체 li에 스타일 */
	li{	border: thin solid gold;	padding: 20px;	}
	/* ul을 절대위치 */
	ul{ position: absolute;	}
	#ulAbs{	background-color: red;	z-index: 1000;	}	
	#ulRel{	background-color: blue; }
	/* 절대위치: absolute */
	
</style>
</head>
<body>
	<h1>HTML POSITION</h1>
	<ul id="ulAbs">
		<li>ABS LI01</li>
		<li>ABS LI02</li>
		<li>ABS LI03</li>
	</ul>
	<ul id="ulRel">
		<li>Rel LI01</li>
		<li>Rel LI02</li>
		<li>Rel LI03</li>
	</ul>
</body>
</html>