<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>amb_test.jsp</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" 
	integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" 
	crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" 
	integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" 
	crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" 
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" 
	crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	function bindList(){
		// 이전의 tr 제거
		$("#ambtable").empty();
		$.get("amb_list", function(data, state) {
			//alert(state);
			if (state == "success") {
				// 성공한 경우
				//alert("GET 성공");
				//alert(data);
				dt = null;
				$("#ambtable").append(
					"<thead><tr><th>제목</th><th>작성자</th><th>날짜</th><th>ip</th></tr></thead>");
				
				for (var i = 0; i < data.length; i++) {
					d = data[i];
					dt = new Date(d.tdate);
					$("#ambtable").append(
						"<tr><td>"+	d.ttitle+"</td>"+	
							"<td>"+d.twriter+"</td>"+
							"<td>"+dt.getFullYear()+"-"+(dt.getMonth() + 1)+"-"+
								dt.getDate()+" "+dt.getHours()+":"+dt.getMinutes()+":"+
								dt.getSeconds()+"</td>"+
							"<td>"+d.tip+"</td></tr>"
					)
				}
			}
		});
	}
	$(function() {
		// 버튼에 이벤트 주기
		bindList();
		$("#h1Title button").click(bindList);	//bindList
		//버튼에 이벤트
		$("#frm_insert button").click(function(){
			var frm=$("#frm_insert")[0];
			if(frm.ttitle.value!="" && frm.twriter.value!=""){
				$.post("amb_insert",{ttitle : frm.ttitle.value, tpw : frm.tpw.value
											, twriter : frm.twriter.value, tip : frm.tip.value})
						.done(function(data, state){
					bindList();
					frm.ttitle.value="";
					frm.tpw.value="";
					frm.twriter.value="";
				});
			}else{
				alert("제목과 작성자를 입력하세요.");
			}
		});
	});
</script>
<style type="text/css">
	li{list-style: none}
</style>
</head>
<body>
	<ul>
		<li>
			<h1>방명록 테스트</h1>
		</li>
		<li>
			<div class="col-sm-10 contact-form">
			<form id="frm_insert">
				<div class="row">
					<div class="col-xs-3 col-md-2 form-group">
						<input class="form-control"  name="ttitle" type="text" placeholder="제목">
					</div>
				</div>
				<div class="row">
					<div class="col-xs-3 col-md-2 form-group">
						<input class="form-control" name=tpw type="password" placeholder="PW">
					</div>
				</div>
				<div class="row">
					<div class="col-xs-3 col-md-2 form-group">
						<input class="form-control" name="twriter" type="text" placeholder="작성자">
					</div>
				</div>
				<div class="row">
					<div class="col-xs-3 col-md-2 form-group">
						<input class="form-control" name="tip" type="text" value="<%=request.getRemoteAddr()%>" readonly>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-3 col-md-2 form-group">
						<button type="button" class="btn btn-info">등록</button>
					</div>
				</div>
				
			</form>
			</div>
		</li>
		<li>
			<h1 id="h1Title">
				<button class="btn btn-success pull-right">방명록 새로고침</button>
			</h1>
		</li>
		<li>
			<table id="ambtable" class="table table-striped table-hover">
				<thead>
					<tr>
						<th>제목</th><th>작성자</th><th>날짜</th><th>ip</th>
					</tr>
				</thead>
		
			</table>
		</li>
	</ul>
</body>
</html>