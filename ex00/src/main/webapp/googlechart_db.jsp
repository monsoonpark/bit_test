<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<meta charset="EUC-KR">
<title>get_arraylist.jsp</title>
<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script>
	// Load the Visualization API and the corechart package.
	google.charts.load('current', {'packages':['corechart']});
	
	// Set a callback to run when the Google Visualization API is loaded.
	google.charts.setOnLoadCallback(drawChart);
	
	// Callback that creates and populates a data table,
	// instantiates the pie chart, passes in the data and
	// draws it.
	var jsondata={
				cols:[ { label: "name", type: "string" },
					{ label: "count", type: "number" } 
				],
				rows:[]
		};
	$(function(){
		
	
		$.ajax({
			url : "top_list",
			timeout : 10000, // 10초
			type : 'get', // get / post 방식
			dataType : 'json', // response 데이터타입
			success : function(data, status) { // 성공함수
				for (var i = 0; i < data.length; i++) {
					d = data[i];
					//alert(d.id+", "+d.count);
					jsondata.rows[i]={ c: [ {v : d.id} , { v: d.count } ] };
				}
				console.log(jsondata);
					
			},
			error : function(request, status, error) { // 에러함수
				alert(error);
			}
		});
	});
	function drawChart() {
		var jdata={
				cols:[ { label: "name", type: "string" },
					{ label: "count", type: "number" } 
				]	,
			rows:[ { c: [ {v : "Mushrooms"} , { v: 3 } ] },
					{ c: [ {v : "Onions"} , { v: 1 } ] },
					{ c: [ {v : "Olives"} , { v: 1 } ] },
					{ c: [ {v : "Zucchini"} , { v: 1 } ] },
					{ c: [ {v : "Pepperoni"} , { v: 2 } ] }
					
				]
		};
		
		//JSON DATA
		//cols: 열 정의, rows: 행 정의
		//c: cell(칸), v: value
		
		
		// Create the data table.
		var data = new google.visualization.DataTable(jsondata);
		
		// Set chart options
		var options = {
			'title' : 'NewsKeywords',
			'width' : 400,
			'height' : 300
		};
		
		// Instantiate and draw our chart, passing in some options.
		var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
		chart.draw(data, options);
	
	}
</script>

</head>

<body>

	<!--Div that will hold the pie chart-->
	<div id="chart_div"></div>
	
</body>

</html>