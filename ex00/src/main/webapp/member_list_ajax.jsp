<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>member_list_ajax.jsp</title>
<script	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	//  var date = new Date(1527257820000);
	//  alert(date.getFullYear());
	function bindList(){
		// 이전의 li들 제거
		$("#ulList").empty();
		$.get("member_list_ajax", function(data, state) {
			//alert(state);
			if (state == "success") {
				// 성공한 경우
				//alert("GET 성공");
				//alert(data);
				dt = null;
				for (var i = 0; i < data.length; i++) {
					d = data[i];
					dt = new Date(d.regdate);
					$("#ulList").append(
							"<li><a href='read?userid="+d.userid+"'>" 
									+ d.username + "</a>("
									+ dt.getFullYear() + "."
									+ (dt.getMonth() + 1) + "."
									+ dt.getDate() + " "
									+ dt.getHours() + ":"
									+ dt.getMinutes() + ":"
									+ dt.getSeconds() + ")</li>");
				}
			}
		});
	}
	function bindList_ub(){
		// 이전의 li들 제거
		$("#ulList").empty();
		$.get("member_list_ajax_ub", function(data, state) {
			//alert(state);
			if (state == "success") {
				// 성공한 경우
				//alert("GET 성공");
				//alert(data);
				dt = null;
				for ( var k in data) {
					console.log(k);
					d=data[k];
					dt = new Date(d.regdate);
					$("#ulList").append(
							"<li><a href='"+k+"'"+d.userid+"'>" 
									+ d.username + "</a>("
									+ dt.getFullYear() + "."
									+ (dt.getMonth() + 1) + "."
									+ dt.getDate() + " "
									+ dt.getHours() + ":"
									+ dt.getMinutes() + ":"
									+ dt.getSeconds() + ")</li>");
				}
			}
		});
	}
	
	$(function() {
		// 버튼에 이벤트 주기
		$("#h1Title button").click(bindList);	//bindList
		$("#h1Title_ub button").click(bindList_ub); //bindList
		
		//버튼에 이벤트
		$("#frmM_insert button").click(function(){
			//alert("register button check");
			//alert($("#frmM_insert")[0].userid.value);
			var frm=$("#frmM_insert")[0];
			$.post("member_insert_ajax",{userid : frm.userid.value, userpw : frm.userpw.value
										, username : frm.username.value, email : frm.email.value })
					.done(function(data, state){
				bindList();
				//alert(data);
				//alert(state);
			});
		});
		
		//버튼 클릭시 p태그 토글
		$("#ulToggle button").on("click",function(){
			var b=$(this);	//button 태그
			//alert(b.next().text());	//javascript - innertext
			var n=b.next();	//button 다음의 p태그
			//$("#ulToggle p").toggle();
			n.fadeToggle(3000);
		});
		//bind ulToggle
		$.get("member_list_ajax", function(data, state) {
			if (state == "success") {
				ul=$("#ulToggle");
				for ( var k in data) {
					var d=data[k];
					dt = new Date(d.regdate);
					li=$("<li></li>");
					btn=$("<button>"+d.username+"("+d.userid+")</button>");
					btn.click(function(){
						var b=$(this);
						var n=b.next();
						n.fadeToggle();
					});
					li.append(btn);
					li.append("<p>"+d.email+"("
							+ dt.getFullYear() + "."
							+ (dt.getMonth() + 1) + "."
							+ dt.getDate() + " "
							+ dt.getHours() + ":"
							+ dt.getMinutes() + ":"
							+ dt.getSeconds() +")");
					ul.append(li);
				}
			}
		});
		var cnt=1;
		$("#ulAnToggle").next().click(function(){
			if(cnt==26){
				cnt=1;
			}
			c=26-cnt;
			cr=Math.ceil(Math.random()*255);
			cg=Math.ceil(Math.random()*255);
			cb=Math.ceil(Math.random()*255);
			ca=Math.random();
			
			//$(this).text("testToggle rgb("+c*10+","+c*10+","+c*10+")");
			//$(this).css("background-color","rgb("+c*10+","+c*10+","+c*10);
			$(this).text("testToggle rgb("+cr+","+cg+","+cb+")");
			$(this).css("background-color","rgba("+cr+","+cg+","+cb+","+ca+")");
			cnt=cnt+1;
		});
	});
</script>
</head>
<body>
	<h1>MEMBER LIST(WITH JQUERY ANOTHER TOGGLE)</h1>
	<ul id="ulAnToggle"></ul>	
	<button>testToggle</button>
	
	<h1>MEMBER LIST(WITH JQUERY TOGGLE)</h1>
	<ul id="ulToggle"></ul>	
		
	<h1>MEMBER INSERT(ajax로 insert)</h1>
	<form id="frmM_insert">
		<ul>
			<li><input name="userid" type="text" placeholder="ID"></li>
			<li><input name=userpw type="password" placeholder="PW"></li>
			<li><input name="username" type="text" placeholder="이름"></li>
			<li><input name="email" type="text" placeholder="email"></li>
			
			<li><button type="button">등록</button></li>
		</ul>
	</form>
	
	<h1 id="h1Title">
		MEMBER LIST
		<button>회원목록 불러오기</button>
	</h1>
	<h1 id="h1Title_ub">
		MEMBER LIST (UriComponentsBuilder 사용)
		<button>회원목록 불러오기</button>
	</h1>
	<ul id="ulList">
	</ul>
</body>
</html>