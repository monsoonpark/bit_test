<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>insert_ajax_mongodb.jsp</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
	// 등록함수
	function registerMember(pFrm) {
		// 
		$.ajax({
			url : "insert_ajax_doc",
			timeout : 10000 // 10초
			,
			type : 'post' // get / post 방식
			,
			dataType : 'json', // response 데이터타입
			headers : {
				"Content-Type" : "application/json",
				"x-HTTP-Method-Override" : "POST"
			},
			data : JSON.stringify({
				"_id" : pFrm._id.value,
				"name" : pFrm.name.value,
				"age" : pFrm.age.value,
				"job" : pFrm.job.value
			}),
			success : function(data, status) { // 성공함수
				alert(data[0]);
			},
			error : function(request, status, error) { // 에러함수
				alert(error);
			}
		});
	}
</script>
</head>
<body>
	<h1>MONGODB INSERT</h1>
	<form>
		<ul>
			<li><input type="text" name="_id" placeholder="예>_id :1"></li>
			<li><input type="text" name="name" placeholder="예>name :홍길동"></li>
			<li><input type="text" name="age" placeholder="예>age :27"></li>
			<li><input type="text" name="job" placeholder="예>job :배우"></li>
			<li>
				<button type="button" onclick="registerMember(this.form);">등록</button>
			</li>
		</ul>
	</form>
</body>
</html>