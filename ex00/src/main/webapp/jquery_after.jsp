<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>jquery_after.jsp</title>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript">
	$(function(){
		//events
		//before
		$("#dvBtns button").click(function(e){
			//alert(this.innerText);
			var txt=this.innerText
			var tget=$("#liTarget");
			if(txt=="before"){	//before 버튼 클릭한 경우
				tget.before("<li>BEFORE LI</li>");
			}else if(txt=="after"){	//after버튼을 클릭한 경우
				tget.after("<li>AFTER LI</li>");
			}
		});
		
	});
</script>
</head>
<body>
	<h1>JQUERY after/ before METHOD</h1>
	<div id="dvBtns">
		<button>before</button>
		<button>after</button>
	</div>
	<ul>
		<li id="liTarget" style="color:red">TARGET</li>
	</ul>
</body>
</html>