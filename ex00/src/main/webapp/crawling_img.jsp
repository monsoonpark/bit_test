<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	$(function(){
		
		$("#takeImgs").on("click", function(){
			$.ajax({
				url : "crawling_img",
				timeout : 10000 // 10초
				,
				type : 'post' // get / post 방식
				,
				dataType : 'json', // response 데이터타입
				data: {pageurl : $("#url").val()},
				success : function(data, status) { // 성공함수
					alert(data[0]);
				},
				error : function(request, status, error) { // 에러함수
				alert(error);
				}
				//JSON.stringify({ id : d._id, name: null})
			});	
			
			
		});
		
	})
</script>
</head>
<body>
	
	<input type="text" id="url">
	<button id="takeImgs">이미지 가져오기</button>
	
	<ul id="ulImgs">
		
	</ul>
</body>
</html>