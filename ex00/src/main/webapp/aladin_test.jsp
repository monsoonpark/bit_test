<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript">
$(function(){
var main = $("div[id^=aladin-bookinfo]"); // aladin-bookinfo로 시작하는 id를 가진 div 엘리먼트를 찾습니다.
//그 엘리먼트가 존재한다면
if(main.length>0) { 
	var id = main.attr("id").substr(16, 10); // ISBN(10자리)부분을 잘라내서 얻습니다 (16칸부터 10개만큼의 문자 얻기)
	var url = "http://www.aladin.co.kr/ttb/api/ItemSearch.aspx?ttbkey=ttbalstn07061542001&Query=aladdin&QueryType=Title&MaxResults=10&start=1&SearchTarget=Book&output=xml&Version=20070901";
	
	// 콜백 함수입니다.
	function bookDisplay(success, data) {
	alert(data);
	   /* 여기서 책 정보를 이용해 구조를 만듭니다! */
	}
	
	// AJAX 요청을 보냅니다. getJSON() 함수는 알라딘 API 대용으로 사용불가.
	$.ajax({
	   url: url,
	   dataType: "json",
	   type:"get"
	});
}
});
</script>
</head>
<body>

	<div id="aladin-bookinfo"></div>
	
</body>
</html>