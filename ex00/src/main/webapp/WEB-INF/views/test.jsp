<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript">
$(function(){
	var bno=130;
	$.getJSON("/replies/all/"+bno, function(data){
		var str="";
		console.log(length);
		
		$(data).each(function(){
			str+="<li data-rno='"+this.rno+"' class='replyLi'>"
				+this.rno+":"+this.replytext
				+"</li>"
		});
		$("#replues").html(str);
	});
	
	$("#replyAddBtn").on("click",function(){
		var replyer=$("#newReplyWriter").val();
		var replytest=$("#newReplyText").val();
		
		$.ajax({
			type:'post',
			url:"/replies",
			headers:{"Content-Type": "application/json", "X-HTTP-METHOD-Override":"POST"},
			dataType:"text",
			data: JSON.stringify({
				bno: bno,
				replyer: replyer,
				replytext: replytext
			}),
			success: function(result){
				if(result=='SUCCESS'){
					alert("등록됨");
					getAllList();
				}
			}
		});
	});
	$("#replies").on("click",".replyLi button",function(){
		var reply=$(this).parent();
		var rno=reply.attr("data-rno");
		var replytext=reply.rext();
		//alert(rno+" : "+replytext);
		$(".modal-title").html(rno);
		$("#replytext").val(replytext);
		$("#modDiv").show("slow");
	});
	$("#replyDelBtn").on("click",function(){
		var rno=$(".modal-title").html();
		var replytext=$("#replytext").val();
		$.ajax({
			type:"delete",
			url:"/replies/"+rno,
			headers:{"Content-Type": "application/json", "X-HTTP-METHOD-Override":"DELETE"}.
			dataType="text",
			success:function(result){
				console.log("result: "+result);
				if(result=='SUCCESS'){
					alert("삭제되었습니다.");
					$("#modDiv").hide("slow");
					getAllList();
				}
			}
		});
	});
	$("#replyModBtn").on("click",function(){
		var rno=$(".modal-title").html();
		var replytext=$("#replytext").val();
		$.ajax({
			type:"put",
			url:"/replies/"+rno,
			headers:{"Content-Type": "application/json", "X-HTTP-METHOD-Override":"PUT"}.
			dataType="text",
			success:function(result){
				console.log("result: "+result);
				if(result=='SUCCESS'){
					alert("수정되었습니다.");
					$("#modDiv").hide("slow");
					getPageList(replyPage);
				}
			}
		});
	});
	var replyPage=1;
	$(".pagination").on("click", "li a", function(){
		event.preventDefault();
		replyPage=$(this).attr("href");
		getPageList(replyPage);
	});
	
});
	
	
function getAllList(){
	$.getJSON("/replies/all/"+bno, function(data){
		var str="";
		console.log(data.length);
		$(data).each(function(){
			str+="<li data-rno='"+this.rno+"' class='replyLi'>"
				+this.rno+":"+this.replytext
				+"<button>MOD</button>"
				+"</li>"
		});
		$("#replues").html(str);
	});
}
function getPageList(page){
	$.getJSON("/replies/all/"+bno, function(data){
		var str="";
		console.log(data.length);
		$(data).each(function(){
			str+="<li data-rno='"+this.rno+"' class='replyLi'>"
				+this.rno+":"+this.replytext
				+"<button>MOD</button>"
				+"</li>"
		});
		$("#replues").html(str);
		printPaging(data.pageMaker);
	});
}
function printPaging(pageMaker){
	var str="";
	if(pageMaker.prev){
		str+="<li><a href='"+(pageMager.startPage-1)+"'> << </a></li>";
	}
	for(var i=pageMaker.startPage, len=pageMaker.endPage; i<= len; i++){
		var strClass=pageMaker.cri.page==i?'class=active':'';
		str+="<li "+strClass+"><a href='"+i+"'>"+i+"</a></li>";
	}
	if(pageMaker.next){
		str+="<li><a href='"+(pageMager.endPage+1)+"'> >> </a></li>";
	}
	$(".pagination").html(str);
}
</script>
<style>
	#modDiv{
		width:300px;
		height:100px;
		background-color: gray;
		position: absolute;
		top:50%;
		left:50%;
		margin-top:-50px;
		margin-left:-150px;
		padding: 10px;
		z-index: 1000;
	}
</style>
</head>
<body>
	<h2>Ajax Test Page</h2>
	<ul id="replies">
				
	</ul>
	
	<div>
		<div>
			Replyer<input type="text" name="replyer" id="newReplyWriter">
		</div>
		<div>
			Reply Text<input type="text" name="replytext" id="newReplyText">
		</div>
		<button id="replyAddBtn">add reply</button>
	</div>
	<div id="modDiv">
		<div class="modal-title"></div>
		<div>
			<input type="text" id="replytext">
		</div>
		<div>
			<button type="button" id="replyModBtn">Modify</button>
			<button type="button" id="replyDelBtn">Delete</button>
			<button type="button" id="closeBtn">Close</button>
		</div>
	</div>
	
	<ul class="pagination">
	
	</ul>
	
</body>
</html>