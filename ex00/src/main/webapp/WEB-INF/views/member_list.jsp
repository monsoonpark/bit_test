<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<table id="testlist" border="1">
		<c:forEach items="${list }" var="m">
			<tr>
				<td>${m.username }</td>
			</tr>
		</c:forEach>
	</table>
	
	<a href="member_insert.jsp">글등록</a>
</body>
</html>