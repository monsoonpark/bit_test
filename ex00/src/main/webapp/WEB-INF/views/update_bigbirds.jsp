<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>update_bigbirds.jsp</title>
<style type="text/css">
	a{
		text-decoration: none;
	}

</style>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script>
	
	// UPDATE FORM SET
	function doUpdate(d){
		// alert(d._id);
		// 초기
		var frm = $("#frmUp");
		frm.empty();
		// append
		frm.append("<input type='hidden' name='id' value='"+d._id+"'><ul><li>"+ d._id +"."+ d.name 
					+"</li><li><input name='anoname' type='text' value='"+ d.anoname 
					+"' required/></li><li><button>UPDATE</button></li></ul>");
	}
	
	function doDelete(d){
		var frm = $("#frmUp");
		frm.empty();
		frm.append("<input type='hidden' name='id' value='"+d._id+"'>");
		frm.attr("action", "delete_bigbirds");
		//frm.submit();
		
		$.ajax({
			url:"delete_bigbirds",
			type : 'post',
			dataType : 'json',
			headers:{"Content-Type": "application/json", "X-HTTP-METHOD-Override":"POST"},
			data: JSON.stringify({ id : d._id, name: null}),
			success : function(result) {
				//alert(result);
			},
			error: function(error){
				alert(error);
			}
		});
		
	}
	function toggleView(d){
		
	}
</script>
</head>
<body>
	<h1>BIGBIRDS TEST</h1>
	
	<form id="frmUp" action="update_bigbirds" method="post">
  	</form>
 	
	<ul>
		<c:forEach items="${list}" var="s">
   			<fmt:parseNumber var="i" integerOnly="true" type="number" value="${s._id}" />
   			<li><a href="update_bigbirds/${s._id }" id="${i}">
 					${i}.${s.name}(무게:${s.size[0]}, 키 : ${s.size[1]} 
  					<c:if test="${ !empty s.anoname}">, 별칭 : ${s.anoname}</c:if> )
   				</a>
		   		<button type="button" onclick="doUpdate({'_id':${s._id}, 'name':'${s.name}', 'anoname' : '${s.anoname}' })">UPDATE</button>
		   		<button type="button" onclick="doDelete({'_id':${s._id}})">DELETE</button>
		   		<button type="button" onclick="toggleView({'_id':${i}})">VIEW DETAIL BY AJAX</button>
			</li>
  		</c:forEach>
 	</ul>
	
	
</body>
</html>