<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>insert_ajax_mongodb.jsp</title>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script>
	
	$(function(){
		
		var list=$("#ulList");
		//$("#birdBtn").on("click",function(){
			$.ajax({
				url: "list_bigbirds_ajax",
				timeout: 10000	//10초
				, type: 'get',
				datType: 'json',
				success: function(data, status){
					list.empty();
					//alert(data);
					data.forEach(function(x){
						list.append("<li>"+x._id+". "+x.name
									+"( 무게: "+x.size[0]+"kg, 키: "
									+x.size[1]+"cm"
									+(x.anoname ? (", 별칭:  "+x.anoname) : "")
									+")</li>");
					});
				},
				erroe: function(request, status, error){
					alert(error);
				}
			});
			
		//});
		
		
	});
	
	// 등록함수
	function updateBirds(pFrm) {
		// 
		$.ajax({
			url : "update_bigbirds",
			timeout : 10000 // 10초
			,
			type : 'post' // get / post 방식
			,
			dataType : 'json', // response 데이터타입
			headers : {
				"Content-Type" : "application/json",
				"x-HTTP-Method-Override" : "POST"
			},
			data : JSON.stringify({
				"_id" : pFrm._id.value,
				"name" : pFrm.name.value,
				"size" : pFrm.size.value,
				"anoname" : pFrm.anoname.value
			}),
			success : function(data, status) { // 성공함수
				list.empty();
				//alert(data);
				data.forEach(function(x){
					list.append("<li>"+x._id+". "+x.name
								+"( 무게: "+x.size[0]+"kg, 키: "
								+x.size[1]+"cm"
								+(x.anoname ? (", 별칭:  "+x.anoname) : "")
								+")</li>");
				});
			},
			error : function(request, status, error) { // 에러함수
				alert(error);
			}
		});
	}
	
	// UPDATE FORM SET
	function doUpdate(d){
		// alert(d._id);
		// 초기
		var frm = $("#frmUp");
		frm.empty();
		// append
		frm.append("<input type='hidden' name='_id' value='"+d._id+"'><ul><li>"+ d._id +"."+ d.name 
					+"</li><li><input name='anoname' type='text' value='"+ d.anoname 
					+"' required/></li><li><button>UPDATE</button></li></ul>");
	}
	
</script>
</head>
<body>
	<h1>BIGBIRDS TEST</h1>
	
	<form>
		<ul>
			<li><input type="text" id="_id" placeholder="id" required></li>
			<li><input type="text" id="name" placeholder="이름"></li>
			<li><input type="text" id="size" placeholder="무게, 키"></li>
			<li><input type="text" id="anoname" placeholder="별칭" required></li>
			<li><button type="button" onclick="updateBirds(this.form);">수정</button></li>
		</ul>
	</form>
	<button type="button" id="birdBtn" >BigBirds List</button>
	
	
	<ul id="ulList">
	</ul>
	<ul>
  		<c:forEach items="${list}" var="s">
   			<fmt:parseNumber var="i" integerOnly="true" type="number" value="${s._id}" />
   			<li>${i}.${s.name}(무게:${s.size[0]}, 키 : ${s.size[1]} 
   				<c:if test="${ !empty s.anoname}">, 별칭 : ${s.anoname}</c:if> )
   		<button type="button" onclick="doUpdate({'_id':${s._id}, 'name':'${s.name}', 'anoname' : '${s.anoname}' })">UPDATE</button>
		</li>
  		</c:forEach>
 	</ul>
	
	<form id="frmUp" action="update_bigbirds" method="post">
  
 	</form>
</body>
</html>