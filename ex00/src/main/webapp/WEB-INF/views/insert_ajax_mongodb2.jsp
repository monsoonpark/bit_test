<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>insert_ajax_mongodb.jsp</title>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script>
	
	$(function(){
		$("#insert").click(function(){
			var id=$("#id").val();
			var name=$("#name").val();
			var age=$("#age").val();
			var job=$("#job").val();
			$.ajax({
				type:'post',
				url:"insert_ajax_doc",
				headers:{"Content-Type": "application/json", "X-HTTP-METHOD-Override":"POST"},
				dataType:"json",
				data: {
					id: id,
					name: name,
					age: age,
					job: job
				},
				success: function(data){
					if(data[0]=='OK'){
						alert("등록완료");
					}
					//alert("등록완료");
				}
			});
		});
	});
	
</script>
</head>
<body>
	<h1>MONGODB TEST</h1>
	
	<ul>
	<li><input type="text" id="id" placeholder="id"></li>
	<li><input type="text" id="name" placeholder="name"></li>
	<li><input type="text" id="age" placeholder="age"></li>
	<li><input type="text" id="job" placeholder="job"></li>
	
	<li><button type="button" id="insert">등록</button>
	</ul>
	
</body>
</html>