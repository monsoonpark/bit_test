<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>test_arrlist.jsp</title>
</head>
<body>
	<h1>MEMBER LIST</h1>
	<ul>
		<c:forEach items="${mems }" var="mem">
			<li>${mem.name }(${mem.age },${mem.address })</li>
		</c:forEach>
	</ul>
</body>
</html>