<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	$(function(){
		
		var frm=$("form[role='form']");
		console.log(frm);	//브라우저 콘솔의 로그
		$("#btnModify").on("click", function(){
			frm.attr("action", "modify_member");
			frm.submit();
		});
		$("#btnList").on("click", function(){
			self.location="/web/member_list_ajax.jsp";	//sendRedirect
		});	
	});
		
</script>
</head>
<body>
	<form role="form" method="post" >
		<ul>
			<li><input name="userid" type="text" value="${memberVO.userid }" readonly></li>
			<li><input name=userpw type="password" value="${memberVO.userpw }"></li>
			<li><input name="username" type="text" value="${memberVO.username }"></li>
			<li><input name="email" type="text" value="${memberVO.email }"></li>
			
			<li>
				<button type="button" id="btnModify">수정</button>
				<button type="button" id="btnList">목록</button>
			</li>
		</ul>
	</form>
</body>
</html>