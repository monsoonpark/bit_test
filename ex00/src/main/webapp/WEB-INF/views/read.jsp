<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	$(function(){
		
		var frm=$("form[role='form']");
		console.log(frm);	//브라우저 콘솔의 로그
		$("#btnModify").on("click", function(){
			frm.attr("action", "modify_member");
			frm.attr("method", "get");
			frm.submit();
		});
		$("#btnRemove").on("click", function(){
			frm.attr("action", "remove_member");
			frm.submit();
		});
		$("#btnList").on("click", function(){
			self.location="/web/member_list_ajax.jsp";	//sendRedirect
		});	
	});
		
</script>
</head>
<body>
	<form role="form" method="post">
		<input type="hidden" name="userid" value="${memberVO.userid }" readonly/>
	</form>
	
	<h1>${memberVO.username }</h1>
	<ul>
		<li>아이디: ${memberVO.userid }</li>
		<li>이메일: ${memberVO.email }</li>
		<li>등록일: <fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${memberVO.regdate }"/></li>
		<li>
			<button type="submit" id="btnModify">Modify</button>
			<button type="submit" id="btnRemove">Remove</button>
			<button type="submit" id="btnList">List</button>
		</li>
	</ul>
	
</body>
</html>