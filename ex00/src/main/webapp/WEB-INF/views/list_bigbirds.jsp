<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>insert_ajax_mongodb.jsp</title>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script>
	
	$(function(){
		
		
	});
	
</script>
</head>
<body>
	<h1>MONGODB TEST</h1>
	
	<ul>
		<c:forEach items="${list }" var="s">
			<li><fmt:formatNumber value="${s._id}" pattern="#" />. ${s.name} 
				(무게: <fmt:parseNumber value="${s.size[0]}" pattern="#"/>kg, 
					키: <fmt:parseNumber value="${s.size[1]}" pattern="#"/>cm
				<c:if test="${!empty s.anoname }">, 별칭: ${s.anoname}</c:if>
				)
			</li>
		</c:forEach>
	</ul>
	
</body>
</html>