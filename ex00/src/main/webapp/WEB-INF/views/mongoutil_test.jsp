<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>mongo_test.jsp</title>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script>
	
	$(function(){
		
	});
</script>
</head>
<body>
	<h1>MONGODB TEST</h1>
	<h2>${dbname }</h2>
	<ul id="cList">
		<c:forEach items="${list }" var="s">
			<li>${s}</li>
		</c:forEach>
	</ul>
	
</body>
</html>