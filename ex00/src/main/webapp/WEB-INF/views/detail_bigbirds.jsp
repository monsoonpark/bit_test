<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>detail_bigbirds.jsp</title>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script>
	
</script>
</head>
<body>
	<h1>BIGBIRDS DETAIL</h1>
	
 	
	<ul>
  		<li>
  			<fmt:parseNumber var="i" integerOnly="trye" type="number" value="${list._id}" />
  			<ul>
  				<li>id: ${i }</li>
  				<li>이름: ${list.name }</li>
  				<li>무게:  ${list.size[0]}kg</li>
  				<li>키: ${list.size[1]}cm</li>
  				<c:if test="${ !empty list.anoname}"><li> 별칭 : ${list.anoname}</li></c:if> 
  				<li><a href="#" onclick="history.back(0);">뒤로가기</a></li>
  			</ul>
  		</li>
 	</ul>
	
	
</body>
</html>