<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>count_testlogs.jsp</title>

<script src="http://code.jquery.com/jquery-latest.js"></script>
<script>
	
	
</script>
</head>
<body>
	<h1>MONGODB COUNT TEST</h1>
	
 	
	<ul>
		<c:forEach items="${list}" var="s">
   			<li>
				${s._id.year } 년 ${s._id.month } 월 ${s._id.hour } 시 : ${s.count } 건	   		
			</li>
  		</c:forEach>
 	</ul>
	
	
</body>
</html>