<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>naver_shop.jsp</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	//숫자 타입에서 쓸 수 있도록 format() 함수 추가
	Number.prototype.format = function() {
		if (this == 0)
			return 0;

		var reg = /(^[+-]?\d+)(\d{3})/;
		var n = (this + '');

		while (reg.test(n))
			n = n.replace(reg, '$1' + ',' + '$2');

		return n;
	};

	// 문자열 타입에서 쓸 수 있도록 format() 함수 추가
	String.prototype.format = function() {
		var num = parseFloat(this);
		if (isNaN(num))
			return "0";

		return num.format();
	};

	// append li
	function appendLis() {
		// 이전 li들 제거
		$("#ulList").empty();
		// get
		$.get("http://localhost:8080/web/naver_proxy2?target=shop&text=%EB%85%B8%ED%8A%B8%EB%B6%81&display=50",
			function(data, state) {
				// 성공확인
				ul = $("#ulList");
				if (state == "success") {
					items = data.items;
					for ( var k in items) {
						item = items[k];
						ul
								.append("<a href='"+ item.link +"' target='_blank'><img src='"+ item.image +"' title='LG전자 올뉴그램 15ZD980-GX30K' /></a><hr /><b>LG전자 올뉴그램 15ZD980-GX30K</b>("
										+ item.lprice.format()
										+ " ~ "
										+ item.hprice.format()
										+ ")");
					}
				}
			});
	}

	// 로드이벤트
	$(function() {
		appendLis();
	})
</script>
</head>
<body>
	<h1>NAVER SHOP</h1>
	<ul id="ulList">
	</ul>
</body>
</html>

