<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>insert_ajax_mongodb.jsp</title>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script>
	
	$(function(){
		var list=$("#ulList");
		$("#birdBtn2").on("click",function(){
			$.ajax({
				url: "/web/list_bigbirds_ajax",
				timeout: 10000	//10초
				, type: 'get',
				datType: 'json',
				success: function(data, status){
					list.empty();
					//alert(data);
					data.forEach(function(x){
						list.append("<li>"+x._id+". "+x.name
									+"( 무게: "+x.size[0]+"kg, 키: "
									+x.size[1]+"cm"
									+(x.anoname ? (", 별칭:  "+x.anoname) : "")
									+")</li>");
						
					});
				},
				erroe: function(request, status, error){
					alert(error);
				}
			});
			
		});
		
		
		$("#birdBtn").on("click",function(){
			$.get("/web/list_bigbirds_ajax", function(data, state){
				if(state == "success"){
					list.text("");
					data.forEach(function(x){
						list.append("<li>"+x._id+". "+x.name
									+"( "+x.size[0]+"kg, "
									+x.size[1]+"cm"
									+", "+x.anoname
									+")</li>");
						
					});
					console.log("완료");
				}
			});
		});

		
	});
	
</script>
</head>
<body>
	<h1>BIGBIRDS TEST</h1>
	
	<ul id="ulList">
		<c:forEach items="${list }" var="s">
			<li><fmt:formatNumber value="${s._id}" pattern="#" />. ${s.name} 
				(무게: <fmt:parseNumber value="${s.size[0]}" pattern="#"/>kg, 
					키: <fmt:parseNumber value="${s.size[1]}" pattern="#"/>cm
				<c:if test="${!empty s.anoname }">, 별칭: ${s.anoname}</c:if>
				)
			</li>
		</c:forEach>
	</ul>
	<button type="button" id="birdBtn">BigBirds List</button>
	<button type="button" id="birdBtn2">BigBirds List</button>
	
</body>
</html>