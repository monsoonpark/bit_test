<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
	li{ list-style: none;}
	a{ text-decoration: none;}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
	function sendInsert(frm){
		var name=frm.name.value;
		var url=frm.url.value;
		//alert(name+", "+ url)
		$.ajax({
		    url : "insertUrl",
		    type : 'post',
			dataType : 'json',
			headers:{"Content-Type": "application/json", "X-HTTP-METHOD-Override":"POST"},
		    data : JSON.stringify({ "name": name, "url": url}),
		    success : function(data, status, xhr) {
		      console.log(data[0]);
		      bindList();
		    },
		    error: function(error) {
		      alert("error= " + error);
		    }
		});
	}
	 
	// BIND CRAWLING LIST
	function bindList() {
		// 
		$.ajax({
			url : "listCrawler",
			timeout : 10000, // 10초
			type : 'get', // get / post 방식
			dataType : 'json', // response 데이터타입
			success : function(data, status) { // 성공함수
				// alert(data[0]._id);
				// 초기
				var ul = $("#ulClist");
				var lis = "";
				var d = null;
				// 이전 li 제거
				ul.empty();
				// li 만들기
				for (var i = 0; i < data.length; i++) {
					d = data[i];
					lis += "<li>"
							+ d.name
							+ " (<a href='"+ d.url +"' target='_blank'>바로가기</a> )</li>";
				}
				// append
				ul.append(lis);
			},
			error : function(request, status, error) { // 에러함수
				alert(error);
			}
		});
	}
	function doCrawling(){
		$.ajax({
			url : "test_crawling",
			timeout : 10000, // 10초
			type : 'get', // get / post 방식
			dataType : 'json',
			success : function(data, status) { // 성공함수
				 alert(data[0].id);
				
			},
			error : function(request, status, error) { // 에러함수
				//alert(request);
				alert(status);
				alert(error);
			}
		});
	}
	
	
	function getFrequencyList() {
		// 
		$.ajax({
			url : "top_list",
			timeout : 10000, // 10초
			type : 'get', // get / post 방식
			dataType : 'json', // response 데이터타입
			success : function(data, status) { // 성공함수
				// alert(data[0]._id);
				// 초기
				var ul = $("#ulKeyList");
				var lis = "";
				var d = null;
				// 이전 li 제거
				ul.empty();
				// li 만들기
				for (var i = 0; i < data.length; i++) {
					d = data[i];
					dt= new Date(d.date);
					lis += "<li>"
							+ d.id+": "+d.count+"건 ("+dt+")"
							+ "</li>";
				}
				// append
				ul.append(lis);
			},
			error : function(request, status, error) { // 에러함수
				alert(error);
			}
		});
	}
	
	$(function(){
		bindList();
		getFrequencyList();
		//bindWord();
	});
</script>
</head>
<body>
	<h1>NEW MAIN URL MANAGER</h1>
	<h2>INSERT FORM</h2>
	<form>
		<ul>
			<li><input type="text" name="name" placeholder="페이지 제목"></li>
			<li><input type="text" name="url" placeholder="페이지 URL"></li>
			<li><button type="button" onclick="sendInsert(this.form);">전송</button></li>
		</ul>
	</form>
	<h2>NEW PAGE</h2>
	<ul id="ulClist">
		
	</ul>
	
	<h2>Crawling Keyword 빈도수 LIST</h2>
	<p><button type="button" onclick="doCrawling();">CRAWLING FREQUENCY</button>
	<p><button type="button" onclick="getFrequencyList();">GET FREQUENCY</button>
	<ul id="ulKeyList">
		
	</ul>
</body>
</html>