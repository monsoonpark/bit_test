<%@page import="java.util.Objects"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLEncoder"%>
<%@ page language="java" contentType="text/xml; charset=UTF-8"
    pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%
StringBuilder urlBuilder = new StringBuilder("http://apis.data.go.kr/9710000/NationalAssemblyInfoService/getJungDangMemberCurrStateList"); /*URL*/
urlBuilder.append("?" + URLEncoder.encode("ServiceKey","UTF-8") + "=I81xROGGmr0CCOnnUpVUyd1qzQ2%2BvUeOOA02QHTrZVQtf8tCq4RAP02BBIwgaPfsUN8U5y7BvqnwPyGdgY%2B6zw%3D%3D"); /*Service Key*/
urlBuilder.append("&" + URLEncoder.encode("numOfRows","UTF-8") + "=" + URLEncoder.encode("300", "UTF-8")); /*한 페이지 결과 수*/
urlBuilder.append("&" + URLEncoder.encode("pageNo","UTF-8") + "=" + URLEncoder.encode("1", "UTF-8")); /*페이지 번호*/
String poly_cd=request.getParameter("poly_cd");
poly_cd=(Objects.isNull(poly_cd) || poly_cd.equals("")) ? "" : poly_cd;
urlBuilder.append("&" + URLEncoder.encode("poly_cd","UTF-8") + "=" + URLEncoder.encode(poly_cd, "UTF-8")); /*지역코드*/

String orig_cd=request.getParameter("orig_cd");
orig_cd=(Objects.isNull(orig_cd) || orig_cd.equals("")) ? "" : orig_cd;
urlBuilder.append("&" + URLEncoder.encode("orig_cd","UTF-8") + "=" + URLEncoder.encode(orig_cd, "UTF-8")); /*지역코드*/

URL url = new URL(urlBuilder.toString());
HttpURLConnection conn = (HttpURLConnection) url.openConnection();
conn.setRequestMethod("GET");
conn.setRequestProperty("Content-type", "application/json");
System.out.println("Response code: " + conn.getResponseCode());
BufferedReader rd;
if(conn.getResponseCode() >= 200 && conn.getResponseCode() <= 300) {
    rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
} else {
    rd = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
}
StringBuilder sb = new StringBuilder();
String line;
while ((line = rd.readLine()) != null) {
    sb.append(line);
}
rd.close();
conn.disconnect();
System.out.println(sb.toString());
out.print(sb.toString());

%>
