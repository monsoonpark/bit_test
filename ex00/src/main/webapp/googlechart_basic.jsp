<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript">

      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);
      google.charts.setOnLoadCallback(drawChart2);
      
      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['Mushrooms', 3],
          ['Onions', 1],
          ['Olives', 1],
          ['Zucchini', 1],
          ['Pepperoni', 2]
        ]);

        // Set chart options
        var options = {'title':'How Much Pizza I Ate Last Night',
                       'width':400,
                       'height':300};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
        
        
      }
      
      function drawChart2(){
    	// 2번 차트
          var data = new google.visualization.DataTable();
          data.addColumn('string', '이름');
          data.addColumn('number', '득점');
          data.addRows([
            ['해리 케인(영국)', 5],
            ['로멜루 루카쿠(벨기에)', 4],
            ['크리스티아누 호날두(포르투갈)', 4],
            ['데니스 체리셰프(러시아)', 3],
            ['디에고 코스타(스페인)', 3]
          ]);
          var options = {'title':'2018 월드컵 득점자',
                  'width':500,
                  'height':300};
          var chart = new google.visualization.PieChart(document.getElementById('chart_div1'));
          chart.draw(data, options);
      }
      
      google.charts.load('current', {
          'packages':['geochart'],
          // Note: you will need to get a mapsApiKey for your project.
          // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
          'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'
        });
        google.charts.setOnLoadCallback(drawRegionsMap);

        function drawRegionsMap() {
          var data = google.visualization.arrayToDataTable([
            ['Country', 'GDP'],
            ['Lu', 120061],
            ['Ch', 86835],
            ['Hm', 84675],
            ['Ax', 80642],
            ['Kr', 32775]
          ]);

          var options = {'title': "2018_06 명목GDP(평균)",
        		  'width':1000,
                  'height':500};

          var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));

          chart.draw(data, options);
        }
    </script>
</head>

<body>
	<!--Div that will hold the pie chart-->
	<div id="chart_div"></div>
	<div id="chart_div1"></div>
	<div id="chart_div2"></div>
	<div id="regions_div" style="width: 900px; height: 500px;"></div>
</body>

</html>