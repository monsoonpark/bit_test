<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>naver_ajaxmethod.jsp</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	$(function(){
		$.ajax({
			url : "naver_proxy.jsp"
			, timeout : 10000	//10초
			, type : 'get' // get / post 방식
			, dataType: 'json'	//response 데이터타입
			, data : {"text" : "java", "display" : 50, "target" : "shop" }
			, success : function(data, status){	//성공함수
				//alert("SUCCESS");
				//alert(data);
				items=data.items;
				ul=$("#ulList");
				for(var i in items){
					ul.append("<li><a href='"+items[i].link+"' target='_blank'>"+items[i].title+"</a></li>")	
				}
			}
			, error :  function(request, status, error){	//에러함수
				alert(error);
			}
		});
	});
</script>
</head>
<body>
	<h1>NAVER API 데이터를 ajax메서드로 GET</h1>
	<ul id="ulList">
		
	</ul>
</body>
</html>