<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript">
	$(function(){
		$.get("naver_proxy",function(data, state){
			if(state=="success"){
				//$("#title").text(data.items[0].title);
				//$("#content").text(data.items[0].description);
				for ( var d in data.items) {
					var i=data.items[d];
					//alert(data.items[d].title);
					$("#news_table tbody").append("<tr><td><a href='"+i.link+"'>"+i.title+"</a></td>"
												+"<td>"+i.pubDate
												+"</td></tr>");
				}
			}
		});
		$("#btnSearch").click(function(){
			$("#news_table tbody tr").remove();
			var locate="naver_proxy?text="+$("#search").val()
								+"&target="+$("#selSearch").val();
								+"&display="+$("#selDisplay").val();
			$.get(locate,function(data, state){
				if(state=="success"){
					//$("#title").text(data.items[0].title);
					//$("#content").text(data.items[0].description);
					for ( var d in data.items) {
						var i=data.items[d];
						//alert(data.items[d].title);
						$("#news_table tbody").append("<tr><td><a href='"+i.link+"'>"+i.title+"</a></td>"
													+"<td>"+i.pubDate
													+"</td></tr>");
					}
				}
			});
		});
	});
</script>
<style>
	li{list-style: none;}
</style>
</head>
<body>
	<select id="selSearch">
		<option value="news">뉴스</option>
		<option value="movie">영화</option>
		<option value="blog">블로그</option>
		<option value="book">책</option>
		<option value="shop">쇼핑</option>
	</select>
	<table id="news_table" border="1">
		<thead>
			<tr>
				<th>제목</th><th>날짜</th>
			</tr>
		</thead>
		<tbody>
		
		</tbody>
	</table>
	<input type="text" id="search"/>
	<select id="selDisplay">
		<option value="5">5</option>
		<option value="10" selected>10</option>
		<option value="20">20</option>
	</select>
	<button id="btnSearch">검색</button>
	
	<br/>
	<br/>
	<h1>NAVER SEARCH</h1>
	<form action="">
		<ul>
			<li><input type="text" name="text" required></li>
			<li>
				<select name="display">
					<option value="10">DISPLAY</option>
					<option value="5">5</option>
					<option value="10" selected>10</option>
					<option value="20">20</option>
					<option value="40">40</option>
				</select>
			</li>
			<li>
				<select name="target">
					<option value="news">검색대상</option>
					<option value="news">뉴스</option>
					<option value="movie">영화</option>
					<option value="blog">블로그</option>
					<option value="book">책</option>
					<option value="shop">쇼핑</option>
				</select>
			</li>
			<li>
				<button>SEARCH</button>
			</li>
			<!-- 목록 -->
			<li>
				<ul id="naver_list">
					<li><a href='' target='_blank'>title</a></li>
				</ul>					
			</li>
		</ul>
	</form>
	
	<table>
		<c:forEach items="${list.items }" var="l">
			<tr><td>${l.title }</td></tr>
		</c:forEach>
	</table>
</body>
</html>