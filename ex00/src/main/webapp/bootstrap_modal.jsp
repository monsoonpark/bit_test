<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>bootstrap_modal.jsp</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<h1>MODAL</h1>
	<!-- MODAL CONTROLL BUTTON -->
	<div>
		<button type="button" class="btn btn-info btn-lg" 
			data-toggle="modal" data-target="#dvModal" data-backdrop="static">모달</button>
	</div>
	<!-- MODAL -->
	<div class="modal fade" id="dvModal" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h1>TEST MODAL	</h1>
					<button type="button" class="btn btn-default" data-dismiss="modal">CLOSE</button>
				</div>
				<div class="modal-body">
					<h1>TEST MODAL CONTENT</h1>
				</div>
				<div class="modal-footer">
					TEST MODAL	WEBSITE
				</div>
				
				
			</div>
		</div>
		</div>
</body>
</html>