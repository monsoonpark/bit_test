<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>eventfunction_test.jsp</title>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript">
	$(function(){
		$("button").on("click",function(e){
			console.log("e.target : "+e.target);
			console.log("e.type : "+e.type);
			console.log("e.timeStamp : "+e.timeStamp);
			//target 이용
			//console.log(e.target.innerText);
			console.log($(e.target).text());
		});
	});
</script>
</head>
<body>
	<h1>EVENT TEST</h1>
	<p>
		<button>돈 BUTTON</button>
		<button>밥 BUTTON</button>
		<button>미인 BUTTON</button>
		<button>미남 BUTTON</button>
		<button>버리는 BUTTON</button>
	</p>
</body>
</html>