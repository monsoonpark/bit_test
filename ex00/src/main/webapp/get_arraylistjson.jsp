<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>get_arraylist.jsp</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	$(function(){
		$.get("do_arraylistjson",function(data, state){
			if(state=="success"){
				for (var i=0; i<data.length; i++) {
					$("#table").append("<tr>");
					$("#table").append("<td>"+data[i].name+"</td>");
					$("#table").append("<td>"+data[i].job+"</td>");
					$("#table").append("<td>"+data[i].age+"</td>");
					$("#table").append("</tr>");
					
				}
			}else{
				alert("실패");
			}
		});		
	});	
</script>
</head>
<body>
	<h1>ArrayList</h1>
	<table border="1" id="table">
		<thead>
			<tr>	
				<th>이름</th><th>직업</th><th>나이</th>
			</tr>
		</thead>
	</table>
</body>
</html>