package org.zerock.service;

import org.springframework.stereotype.Service;

@Service
public class TestServiceImpl {

	public int testInt() throws Exception {
		return 777;
	}
	
	public String testString() throws Exception {
		return "홍길동";
	}
}
