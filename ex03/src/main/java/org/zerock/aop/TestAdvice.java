package org.zerock.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class TestAdvice {

	private static final Logger logger = LoggerFactory.getLogger(TestAdvice.class);

	@Around("execution(* org.zerock.service.TestService*.*(..))")
	public Object timeLog(ProceedingJoinPoint pjp) throws Throwable {
		//before
		logger.info("TestService의 메서드 "+pjp.getSignature().getName()+" 를 출력");

		//메서드 실행
		Object result = pjp.proceed();
		
		//after
		//판단
		String msg=null;
		if(result instanceof String) {
			msg=String.format("String testString() : \"%s\"을 리턴", result) ;
		}else {
			msg=String.format("Int testInt() : \"%s\"을 리턴", result) ;
		}
		logger.info(msg);

		return result;
	}
	
	//@Before("execution(* org.zerock.service.TestService*.*(..))")
	public void methodName(JoinPoint jp) throws Throwable {
		logger.info(jp.getSignature().getName());
	}

	//@After("execution(* org.zerock.service.TestService*.*(..))")
	public void returnValue() throws Throwable {
		
	}

}
