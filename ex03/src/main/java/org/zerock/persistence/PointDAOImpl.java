package org.zerock.persistence;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

@Repository
public class PointDAOImpl implements PointDAO {

	@Inject
	private SqlSession session;
	
	private static String namespace="org.zerock.mappers.PointMapper";
	
	@Override
	public void updatePoin(String uid, int point) throws Exception {
		Map<String, Object> paramMap=new HashMap<>();
		paramMap.put("uid", uid);
		paramMap.put("point", point);
		
		session.update(namespace+".updatePoint", paramMap);
	}

}
