<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="../include/header.jsp" %>

	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">SUCCESS PAGE</h3>
		</div>
		<!-- /.box-body -->
		<div class="box-body">SUCCESS!</div>
		<!-- /.box-footer -->
		<div class="box-footer">Footer</div>
	</div>
	
<%@ include file="../include/footer.jsp" %>