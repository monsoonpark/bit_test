<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>hidden_iframe.jsp</title>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript">

	
	//inner.jsp 에서 호출할 함수
	function callMe(){
		alert("inner.jsp에서 호출");
	}
</script>
<style type="text/css">
	/* iframe{	visibility: hidden;	}*/
</style>
</head>
<body>
	<h1>HIDDEN IFRAME TEST</h1>
	<ul>
		<li><a href="http://www.nate.com" target="ifHidden">iframe에 nate.com 보여주기</a>
		<li><a href="http://www.cyworld.com" target="ifHidden">iframe에 cyworld.com 보여주기</a>
		<li><a href="http://www.daum.net" target="ifHidden">iframe에 daum.net 보여주기</a>
		<li><a href="inner.jsp" target="ifHidden">iframe에 inner.jsp 보여주기</a>
	</ul>
	<iframe name="ifHidden">
	
	</iframe>
	
</body>
</html>