<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>iframe_test.jsp</title>
<style type="text/css">
	iframe{
		width: 800px;
		height: 200px;	
		border: gold solid thick;
	}
</style>
</head>
<body>
	<h1>iframe : 페이지내에 다른 페이지를 삽입하는 것</h1>
	
	<ul>
		<li><h2>NATE</h2></li>
		<li><iframe src="http://www.nate.com"></iframe></li>
		<li><h2>CYWORLD</h2></li>
		<li><iframe src="http://www.cyworld.com"></iframe></li>
		<li><h2>DAUM</h2></li>
		<li><iframe src="http://www.daum.net"></iframe></li>
	</ul>
</body>
</html>