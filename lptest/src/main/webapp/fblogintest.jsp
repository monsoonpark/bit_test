<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

	<script>
		/*
		//This is called with the results from from FB.getLoginStatus().
		function statusChangeCallback(response) {
			console.log('statusChangeCallback');
			console.log(response);
			// response 객체는 현재 로그인 상태를 나타내는 정보를 보여준다. 
			// 앱에서 현재의 로그인 상태에 따라 동작하면 된다. 
			// FB.getLoginStatus().의 레퍼런스에서 더 자세한 내용이 참조 가능하다.
			if (response.status === 'connected') {
				testAPI();
			} else if (response.status === 'not_authorized') {
				// 페이스북에는 로그인 했으나, 앱에는 로그인이 되어있지 않다.
				document.getElementById('status').innerHTML = 'Please log '
						+ 'into this app.';
			} else {
				// 페이스북에 로그인이 되어있지 않다. 따라서, 앱에 로그인이 되어있는지 여부가 불확실하다.
				document.getElementById('status').innerHTML = 'Please log '
						+ 'into Facebook.';

			}
		}
		*/
		//이 함수는 누군가가 로그인 버튼에 대한 처리가 끝났을 때 호출된다. 
		// onlogin 핸들러를 아래와 같이 첨부하면 된다.
		/*
		function checkLoginState() {
			FB.getLoginStatus(function(response) {
				statusChangeCallback(response);
			});
		}
		*/
		/*
		window.fbAsyncInit = function() {
			FB.init({
				appId : '{1250201825110378}',
				cookie : true,
				// 쿠키가 세션을 참조할 수 있도록 허용 
				xfbml : true,
				// 소셜 플러그인이 있으면 처리 
				version : 'v2.1' // 버전 2.1 사용 
			});
			FB.getLoginStatus(function(response) {
				statusChangeCallback(response);
				console.log(response.authResponse.userID);
			});

		}
		*/
		
		(function(d, s, id) {
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id))
						return;
					js = d.createElement(s);
					js.id = id;
					js.src = 'https://connect.facebook.net/ko_KR/sdk.js#xfbml=1&version=v3.0&appId=1250201825110378&autoLogAppEvents=1';
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'))
		
		var finished_rendering = function() {
			console.log("finished rendering plugins");
			var spinner = document.getElementById("spinner");
			spinner.removeAttribute("style");
			spinner.removeChild(spinner.childNodes[0]);
		}
		
		FB.Event.subscribe('xfbml.render', finished_rendering);
	</script>
	<!-- <div id="spinner"
		style="background: #4267b2; border-radius: 5px; color: white; height: 40px; text-align: center; width: 250px;">
		Loading</div> -->
		<!-- <div class="fb-login-button" data-max-rows="1" data-size="large"
			data-button-type="continue_with" data-use-continue-as="true"></div> -->
	

		<div id="status"></div>
		<div class="fb-login-button" data-max-rows="1" data-size="large"
			data-button-type="continue_with" data-show-faces="true"
			data-auto-logout-link="true" data-use-continue-as="true"></div>
</body>
</html>