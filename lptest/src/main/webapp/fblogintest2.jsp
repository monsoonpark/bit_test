<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

	<script>
	
		window.fbAsyncInit = function() {
			FB.init({
				appId : '1250201825110378',
				autoLogAppEvents : true,
				xfbml : true,
				version : 'v3.0'
			});
			FB.ui({
				  method: 'share_open_graph',
				  action_type: 'og.likes',
				  action_properties: JSON.stringify({
				    object:'https://developers.facebook.com/docs/javascript/examples',
			  })
			}, function(response){
			  // Debug response (optional)
			  console.log(response);
			});
			FB.login(function(response) {
			    if (response.authResponse) {
			     console.log('Welcome!  Fetching your information.... ');
			     FB.api('/me', function(response) {
			       console.log('Good to see you, ' + response.name + '.');
			     });
			    } else {
			     console.log('User cancelled login or did not fully authorize.');
			    }
			}, {scope: 'public_profile,email'});
			FB.getLoginStatus(function(response) {
			    statusChangeCallback(response);
			});
			    
		};

		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {
				return;
			}
			js = d.createElement(s);
			js.id = id;
			js.src = "https://connect.facebook.net/en_US/sdk.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
		
		
		function statusChangeCallback(response) {
			console.log('statusChangeCallback');
			console.log(response);
			// The response object is returned with a status field that lets the
			// app know the current login status of the person.
			// Full docs on the response object can be found in the documentation
			// for FB.getLoginStatus().
			if (response.status === 'connected') {
				// Logged into your app and Facebook.
				testAPI();
			} else {
				// The person is not logged into your app or we are unable to tell.
				document.getElementById('status').innerHTML = 'Please log '
						+ 'into this app.';
			}
		}
		
		function testAPI() {
		    console.log('Welcome!  Fetching your information.... ');
		    FB.api('/me', {fields: 'email,name'}, function(response) {
		      console.log('Successful login for: ' + response.name+", "+response.email);
		      document.getElementById('status').innerHTML =
		        'Thanks for logging in, ' + response.name + '!';
		    });
		    
		  }
	</script>
	<!-- <div id="spinner"
		style="background: #4267b2; border-radius: 5px; color: white; height: 40px; text-align: center; width: 250px;">
		Loading</div> -->
		<!-- <div class="fb-login-button" data-max-rows="1" data-size="large"
			data-button-type="continue_with" data-use-continue-as="true"></div> -->
	

		<div id="status"></div>
		<div class="fb-login-button" data-max-rows="1" data-size="large"
			data-button-type="continue_with" data-show-faces="true"
			data-auto-logout-link="true" data-use-continue-as="true"></div>
</body>
</html>