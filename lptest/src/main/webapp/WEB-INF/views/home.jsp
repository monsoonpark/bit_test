<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
	<title>Home</title>
</head>
<body>
<h1>
	Hello world!  
</h1>
	
<P>  The time on the server is ${serverTime}. </P>
<ul>
<c:forEach items="${mongo }" var="m">
	<li><a href="${m.link }">${m.text } (${m.date })</a></li>
</c:forEach>
</ul>
</body>
</html>
