<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style type="text/css">
 body{
 	padding-top: 200px;
 }
 img {
 	width:100px;
 	height:35px;
 	margin-left: 12px;
 }
 .btn-block{
 	background-color: #4286f4;
 	color: white;
 }
</style>
</head>
<body>

<div class="container">
    <div class="row">
		<div class="col-md-4 col-md-offset-4">
    		<div class="panel panel-default">
			  	<div class="panel-heading">
			    	<h3 class="panel-title">로그인</h3>
			 	</div>
			  	<div class="panel-body">
			    	<form accept-charset="UTF-8" role="form">
                    <fieldset>
			    	  	<div class="form-group">
			    		    <input class="form-control" placeholder="E-mail" name="email" type="text">
			    		</div>
			    		<div class="form-group">
			    			<input class="form-control" placeholder="Password" name="password" type="password" value="">
			    		</div>
			    		<div class="checkbox">
			    	    	<label>
			    	    		<input name="remember" type="checkbox" value="Remember Me"> 아이디 저장
			    	    	</label>
			    	    </div>
			    		<input class="btn btn-lg btn-block" type="submit" value="Login">
			    	</fieldset>
			      	</form>
			    </div>
			   	<div class="panel">
			   		<img src="resources/img/naver_login.PNG"/>
			   		<img src="resources/img/페북.png"/>
			   		<img src="resources/img/구글.png"/>
			   	</div> 
			   
			</div>
		</div>
	</div>
</div>

</body>
</html>